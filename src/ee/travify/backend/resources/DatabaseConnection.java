package ee.travify.backend.resources;

import ee.travify.backend.Configuration;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DatabaseConnection {
    private static Connection connection = null;

    public static Connection getConnection() {
        Configuration config = new Configuration(); // load configuration

        Properties connectionProperties = new Properties();
        connectionProperties.put("user", config.databaseUser);
        connectionProperties.put("password", config.databasePassword);
        loadDriver();

        if (connection == null) {
            try {
                connection = DriverManager.getConnection(config.databaseUrl, connectionProperties);
            } catch (SQLException e) {
                System.out.println("Error on creating database connection: " + e.getMessage());
                e.printStackTrace();
            }
        }

        return connection;
    }

    private static void loadDriver() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver").getConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
                | NoSuchMethodException | SecurityException | ClassNotFoundException e) {
            System.out.println("Error on loading driver: " + e.getMessage());
        }

    }

    public static void closeConnection(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                System.out.println("Error on closing database connection: " + e.getMessage());
            }
        } else {
            try {
                DatabaseConnection.connection.close();
            } catch (SQLException e) {
                System.out.println("Error on closing database connection: " + e.getMessage());
            }
        }
    }
}
