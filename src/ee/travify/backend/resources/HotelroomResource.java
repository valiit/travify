package ee.travify.backend.resources;

import ee.travify.backend.dao.Hotel;
import ee.travify.backend.dao.Hotelroom;


import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Set;
import java.util.TreeSet;

public class HotelroomResource {


    public static Set<Hotelroom> getAllHotelrooms() {
        return getAllHotelRoomsFromSqlQuery("SELECT * FROM hotelrooms");
    }

    public static Hotelroom getHotelroomById(long hotelroomId) {
    	if (hotelroomId != 0) {
        return getAllHotelRoomsFromSqlQuery("SELECT * FROM hotelrooms WHERE id=" + hotelroomId).first();
    	} else return null;
    }

    public static String saveHotelRoom(Set<Hotelroom> rooms) {
        String sqlQuery = "INSERT INTO hotelrooms (room_name, places, hotel, price) VALUES";

        for (Hotelroom hotelroom: rooms) {
            sqlQuery += String.format("('%s', %s, %s, %s),",
                    hotelroom.getRoom_name(),
                    hotelroom.getPlaces(),
                    hotelroom.getHotel().getId(),
                    hotelroom.getPrice());
        }
        sqlQuery = sqlQuery.substring(0, sqlQuery.length()-1);
        System.out.println(sqlQuery);

        try (Statement stmt = DatabaseConnection.getConnection().createStatement()) {
            stmt.executeUpdate(sqlQuery);
        } catch (SQLException e) {
            System.out.println("Error on saving hotel room: " + e.getMessage());
            return "ERROR";
        }
        return "OK";
    }

    public static TreeSet<Hotelroom> getAvailableInBetween(long cityId, String fromDate, String toDate){
        TreeSet<Hotelroom> allRooms = new TreeSet<>();
        try {
            CallableStatement storedProc = DatabaseConnection.getConnection().prepareCall("call get_available_rooms_in_city_in_between_dates (?,?,?)");
            storedProc.setLong(1, cityId);
            storedProc.setString(2, fromDate);
            storedProc.setString(3, toDate);
            boolean hadResults = storedProc.execute();

            while (hadResults) {
                ResultSet results = storedProc.getResultSet();
                while (results.next()) {
                    Hotelroom hotelroom = new Hotelroom()
                            .setId(results.getLong("room_id"))
                            .setRoom_name(results.getString("room_name"))
                            .setPlaces(results.getLong("places"))
                            .setPrice(results.getBigDecimal("price"))
                            .setHotel(HotelResource.getHotelById(results.getLong("hotel_id")));
                    allRooms.add(hotelroom);
                }
                results.close();
                hadResults = storedProc.getMoreResults();
            }

        } catch (SQLException e) {
            System.out.println("Error on getting hotel rooms: " + e.getMessage());
        }

        return allRooms;
    }
    private static TreeSet<Hotelroom> getAllHotelRoomsFromSqlQuery(String sqlQuery) {
    	System.out.println(sqlQuery);
        TreeSet<Hotelroom> allHotelrooms = new TreeSet<>();
        try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery)) {
            while (results.next()) {
                allHotelrooms.add(new Hotelroom()
                        .setId(results.getLong("id"))
                        .setRoom_name(results.getString("room_name"))
                        .setPlaces(results.getLong("places"))
                        .setHotel(new Hotel().setId(results.getLong("hotel")))
                        .setPrice(results.getBigDecimal("price")));
            }
        } catch (SQLException e) {
            System.out.println("Error on getting hotelrooms set: " + e.getMessage());
        }
        return allHotelrooms;
    }

}

