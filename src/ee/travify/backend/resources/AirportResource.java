package ee.travify.backend.resources;

import ee.travify.backend.dao.Airport;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;
import java.util.TreeSet;

public class AirportResource {

    public static TreeSet<Airport> getAllAirports() {
        return getAirportsFromSqlQuery("SELECT * FROM airports");

    }

    public static Airport getAirportById(Long airportId) {
        if (airportId != 0) {
            return getAirportsFromSqlQuery("Select * from airports where id=" + airportId).first();
        } else {
            return null;
        }
    }


    public static Set<Airport> getAirportsByCityId(long cityId) {
        return getAirportsFromSqlQuery("Select * from airports where in_city=" + cityId);
    }

    public static void updateAirport(Airport airport) {
        String sqlQuery = "UPDATE airports SET airport_code='" + airport.getAirport_code()
                + "', country='" + airport.getCountry()
                + "', in_region='" + airport.getIn_region()
                + "', in_city='" + airport.getIn_city()
                + "' WHERE id = " + airport.getId();

        try {
            Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
            if (code != 1) {
                throw new SQLException("Something went wrong on updating airport");
            }

        } catch (SQLException e) {
            System.out.println("Error on getting airports set: " + e.getMessage());
        }

    }

    private static TreeSet<Airport> getAirportsFromSqlQuery(String sqlQuery) {
        TreeSet<Airport> allAirports = new TreeSet<>();
        try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery)) {
            while (results.next()) {
                allAirports.add(newAirportFromResults(results));
            }
        } catch (SQLException e) {
            System.out.println("Error on getting airports set: " + e.getMessage());
        }
        return allAirports;

    }

    private static Airport newAirportFromResults(ResultSet results) throws SQLException {
        return new Airport()
                .setId(results.getLong("id"))
                .setAirport_code(results.getString("airport_code"))
                .setCountry(CountryResource.getCountryById(results.getLong("country")))
                .setIn_region(RegionsResource.getRegionsById(results.getLong("in_region")))
                .setIn_city(CityResource.getCityById(results.getLong("in_city")));
    }

}
