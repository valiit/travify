package ee.travify.backend.resources;

import ee.travify.backend.dao.Event;
import ee.travify.backend.dao.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Set;
import java.util.TreeSet;

public class EventResource {

    public static Set<Event> getAllEvents(User organizer) {
        return getAllEventsFromSqlQuery("SELECT * FROM events WHERE organizer = " + organizer.getId());
    }

    public static Event getEventById(long eventId) {
        return getAllEventsFromSqlQuery("SELECT * FROM events WHERE id=" + eventId).first();
    }

    public static Event addNewEvent(Event event) {

         String sqlQuery = String.format(
                "INSERT INTO events (title, venue, fromDate, toDate, organizer, arrivalFlex, departureFlex, budget, region) VALUES ('%s', %s, '%s', '%s', %s, %s, %s, %s, '%s' )",
                event.getTitle(),
                event.getVenue(),
                new Timestamp(event.getFromDate().getTime()),
                new Timestamp(event.getToDate().getTime()),
                event.getOrganizer(),
         		event.getArrivalFlex(),
         		event.getDepartureFlex(),
                event.getBudget(),
                event.getRegion().getId());
        System.out.println(sqlQuery);

        try (Statement statement = DatabaseConnection.getConnection().createStatement()) {
            statement.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
            ResultSet resultSet = statement.getGeneratedKeys();
            while (resultSet.next()) {
                event.setId(resultSet.getLong(1));
            }
        } catch (SQLException e) {
            System.out.println("Error on adding new event: " + e.getMessage());
        }
        return event;
    }

    public static void updateEvent(Event event) {
        String sqlQuery = String.format("UPDATE events SET title='%s', fromDate='%s', toDate='%s', budget=%s WHERE id=%d", event.getTitle(), event.getFromDate(), event.getToDate(), event.getBudget(), event.getId());
        try {
            Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
            if (code != 1) {
                throw new SQLException("Something went wrong on updating event!");
            }
        } catch (SQLException e) {
            System.out.println("Error on getting event: " + e.getMessage());
        }
    }

    public static void deleteEvent(Event event) {
        String sqlQuery = "DELETE FROM events WHERE id=" + event.getId();
        try {
            Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
            if (code != 1) {
                throw new SQLException("Something went wrong on deleting event!");
            }
        } catch (SQLException e) {
            System.out.println("Error on getting event: " + e.getMessage());
        }
    }
    private static TreeSet<Event> getAllEventsFromSqlQuery(String sqlQuery) {
        System.out.println(sqlQuery);
        TreeSet<Event> allEvents = new TreeSet<>();
        try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery)) {
            while (results.next()) {
                Event event = new Event()
                        .setOrganizer(results.getLong("organizer"))
                        .setId(results.getLong("id"))
                        .setTitle(results.getString("title"))
                        .setFromDate(results.getTimestamp("fromDate"))
                        .setRegion(RegionsResource.getRegionsById(results.getLong("region")))
                        .setToDate(results.getTimestamp("toDate"))
                        .setBudget(results.getBigDecimal("budget"));
                event.setParticipants(ParticipantResource.getParticipantsByEvent(event));

                if (results.getLong("venue")!=0) {
                    event.setVenue(VenuesResource.getVenuesById(results.getLong("venue")));
                }
                allEvents.add(event);
            }
        } catch (SQLException e) {
            System.out.println("Error on getting events set: " + e.getMessage());
        }
        return allEvents;

    }
}
