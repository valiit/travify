package ee.travify.backend.resources;

import ee.travify.backend.dao.Event;
import ee.travify.backend.dao.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.TreeSet;

public class UserResource {

	public static TreeSet<User> getAllUsers() {
		return getAllUsersFromSqlQuery("SELECT * FROM users");
	}

	public static TreeSet<User> getAllUsersByEvent(Event event) {
		return getAllUsersFromSqlQuery("SELECT " +
				"participants.id AS participant_id, " +
				"participants.userId AS user_id, " +
				"users.username," +
				"users.password, " +
				"users.first_name," +
				"users.last_name," +
				"users.email," +
				"users.def_location, " +
				"users.id " +
				"FROM participants " +
				"JOIN " +
				"users ON participants.userId = users.id " +
				"WHERE eventId=" + event.getId());
	}

	public static User getUserById(long userId) {
		return getAllUsersFromSqlQuery("SELECT * FROM users WHERE id=" + userId).first();
	}

	public static boolean checkIfUserExists(User user) {
		TreeSet<User> users = getAllUsersFromSqlQuery("SELECT * FROM users WHERE first_name= '" + user.getFirst_name()
				+ "' AND last_name= '" + user.getLast_name() + "' AND email= '" + user.getEmail() + "'");
		return users.size() == 0 ? false : true;
	}

	public static User userLogin(String userName, String password) {
		return getAllUsersFromSqlQuery("SELECT * FROM users WHERE username='" + userName + "' AND password=PASSWORD('"
				+ password + "') LIMIT 0,1").first();
	}

	public static User addNewUser(User user) {
		String sqlQuery = String.format(
				"INSERT INTO users (username, password, email, def_location, first_name, last_name) "
						+ "VALUES ('%s',PASSWORD('%s'),'%s',%s,'%s','%s')",
				user.getUsername(),
				user.getPassword(),
				user.getEmail(),
				user.getDef_location().getId(),
				user.getFirst_name(),
				user.getLast_name());

		try (Statement statement = DatabaseConnection.getConnection().createStatement()) {
			statement.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			ResultSet resultSet = statement.getGeneratedKeys();
			while (resultSet.next()) {
				user.setId(resultSet.getLong(1));
			}
		} catch (SQLException e) {
			System.out.println("Error on adding new User: " + e.getMessage());
		}
		return user;
	}

	public static void updateUser(User user) {
		String sqlQuery = String.format("UPDATE users SET " +
				"username='%s'," +
				"password=PASSWORD('%s')," +
				"email='%s'," +
				"def_location=%s," +
				"first_name='%s'," +
				"last_name='%s'  " +
				"WHERE id=%s",
				user.getUsername(),
				user.getPassword(),
				user.getEmail(),
				user.getDef_location().getId(),
				user.getFirst_name(),
				user.getLast_name(),
				user.getId());
		try {
			Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
			if (code != 1) {
				throw new SQLException("Something went wrong on updating user!");
			}
		} catch (SQLException e) {
			System.out.println("Error on getting user: " + e.getMessage());
		}
	}

	public static void deleteUser(User user) {
		String sqlQuery = "DELETE FROM users WHERE id=" + user.getId();
		try {
			Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
			if (code != 1) {
				throw new SQLException("Something went wrong on deleting user!");
			}
		} catch (SQLException e) {
			System.out.println("Error on getting user: " + e.getMessage());

		}
	}

	private static TreeSet<User> getAllUsersFromSqlQuery(String sqlQuery) {
		TreeSet<User> allUsers = new TreeSet<>();
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery)) {
			while (results.next()) {
				allUsers.add(new User()
						.setId(results.getLong("id"))
						.setUsername(results.getString("username"))
						.setPassword(results.getString("password"))
						.setEmail(results.getString("email"))
						.setDef_location(CityResource.getCityById(results.getLong("def_location")))
						.setFirst_name(results.getString("first_name"))
						.setLast_name(results.getString("last_name")));
			}
		} catch (SQLException e) {
			System.out.println("Error on getting users set: " + e.getMessage() + "\n" + sqlQuery);
		}
		return allUsers;
	}

}
