package ee.travify.backend.resources;


import ee.travify.backend.dao.Event;
import ee.travify.backend.dao.Participant;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Set;
import java.util.TreeSet;

public class ParticipantResource {
	public static TreeSet<Participant> getAllParticipants() {
		return getAllParticipantsFromSqlQuery("SELECT * FROM participants");
	}

	public static Participant getParticipantById(long participantId) {
		return getAllParticipantsFromSqlQuery("SELECT * FROM participants WHERE id= " + participantId).first();
	}

	public static TreeSet<Participant> getParticipantsByEvent(Event event) {
		return getAllParticipantsFromSqlQuery("SELECT * FROM participants WHERE eventId = " + event.getId());
	}

	public static void deleteParticipant(long participantId) {
		String sqlQuery = "DELETE from participants WHERE id =" + participantId;
		try {
			Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
			if (code != 1) {
				throw new SQLException("Something went wrong on deleting participant");
			}
		} catch (SQLException e) {
			System.out.println("Error deleting participant: " + e.getMessage());
		}
	}

	public static void updateParticipant(Participant participant) {
		String sqlQuery = String.format("UPDATE participants SET " +
						"hotelbooking='%s', " +
						"arrivalFlightBooking='%s', " +
						"departureFlightBooking='%s', " +
						"userId='%s', " +
						"eventId='%s' " +
						"startLocation='%s' " +
						"WHERE id = %s",
				participant.getHotelbooking().getId(),
				participant.getArrivalFlightBooking().getId(),
				participant.getDepartureFlightBooking().getId(),
				participant.getUser().getId(),
				participant.getEventId(),
				participant.getStartLocation().getId(),
				participant.getId());
		try {
			Integer code =
					DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
			if (code != 1) {
				throw new SQLException("Something went wrong on updating participants");
			}
		} catch (SQLException e) {
			System.out.println("Error updating participant information: " + e.getMessage());
		}
	}

	public static boolean addParticipant(Participant participant) {

		String sqlQuery = String.format
				("INSERT INTO participants (userId, eventId, hotelbooking, arrivalFlightBooking, departureFlightBooking, startLocation) VALUES (%s, %s, %s, %s, %s, %s)",

						participant.getUser().getId(),
						participant.getEventId(),
						participant.getHotelbooking().getId() == 0 ? "NULL" : participant.getHotelbooking().getId(),
						participant.getArrivalFlightBooking().getId()==0 ? "NULL" : participant.getArrivalFlightBooking().getId(),
						participant.getDepartureFlightBooking().getId()  == 0 ? "NULL" :participant.getDepartureFlightBooking().getId(),
						participant.getStartLocation().getId());

		System.out.println(sqlQuery);

		try (Statement stmt = DatabaseConnection.getConnection().createStatement()) {
            stmt.executeUpdate(sqlQuery);
        } catch (SQLException e) {
            System.out.println("Error on adding participants: " + e.getMessage());
            return false;
        }
        return true;

	}

    public static boolean addMultipleParticipants(Set<Participant> multipleParticipants) {
		boolean retVal = true;
		String sqlQuery = "INSERT INTO participants (userId, startLocation, eventId) VALUES ";
		for (Participant participant : multipleParticipants) {
			sqlQuery += String.format("(%s, %s, %s),",
					participant.getUser().getId(),
					participant.getStartLocation().getId(),
					participant.getEventId());
		}
		sqlQuery = sqlQuery.substring(0, sqlQuery.length() - 1);

		try (Statement stmt = DatabaseConnection.getConnection().createStatement()) {
			stmt.executeUpdate(sqlQuery);
		} catch (SQLException e) {
			System.out.println("Error on adding multiple participants: " + e.getMessage());
			retVal = false;
		}
		return retVal;
	}

	private static TreeSet<Participant> getAllParticipantsFromSqlQuery(String sqlQuery) {
        System.out.println(sqlQuery);TreeSet<Participant> allParticipants = new TreeSet<>();
        try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery)) {
            while (results.next()) {
                allParticipants.add(new Participant()
                        .setId(results.getLong("id"))
                        .setUser(UserResource.getUserById(results.getLong("userId")))
                        .setEventId(results.getLong("eventId")) // do NOT ask EventResource to build event from this ID// - circular reference! Stack overflow! SPOOKY!
                        .setHotelbooking(HotelroomResource.getHotelroomById(results.getLong("hotelbooking")))
                        .setArrivalFlightBooking(FlightsResource.getFlightById(results.getLong("arrivalFlightBooking")))
                        .setDepartureFlightBooking(FlightsResource.getFlightById((results.getLong("departureFlightBooking"))))
                .setStartLocation(CityResource.getCityById(results.getLong("startLocation"))));
            }
        } catch (SQLException e) {
            System.out.println("Error on getting participants set: " + e.getMessage());
        }
        return allParticipants;
    }
}