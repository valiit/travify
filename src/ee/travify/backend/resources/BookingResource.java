package ee.travify.backend.resources;

import ee.travify.backend.dao.Booking;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Set;
import java.util.TreeSet;

public class BookingResource {

    public static Set<Booking> getAllBookings() {
        return getAllBookingsFromSqlQuery("SELECT * FROM bookings");
    }

    public static Booking getBookingById(Long bookingId) {
        return getAllBookingsFromSqlQuery("Select * from bookings where id=" + bookingId).first();
    }

    public static Booking addNewBooking(Booking booking) {
        String sqlQuery = "INSERT INTO bookings (room, fromdate, todate, venue) "
                + "VALUES ('" + booking.getRoom() + "', '" + booking.getFromdate() + "', '" + booking.getTodate() + "', '"
                + booking.getVenue() + "')";
        try (Statement statement = DatabaseConnection.getConnection().createStatement()) {
            statement.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
            ResultSet resultSet = statement.getGeneratedKeys();
            while (resultSet.next()) {
                booking.setId(resultSet.getLong(1));
            }
        } catch (SQLException e) {
            System.out.println("Error on adding new booking: " + e.getMessage());
        }
        return booking;
    }

    public static void updateBooking(Booking booking) {
        String sqlQuery = "UPDATE bookings SET room='" + booking.getRoom()
                + "', fromdate='" + booking.getFromdate()
                + "', todate='" + booking.getTodate()
                + "', venue='" + booking.getVenue()
                + "' WHERE id = " + booking.getId();

        try {
            Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
            if (code != 1) {
                throw new SQLException("Something went wrong on updating booking");
            }

        } catch (SQLException e) {
            System.out.println("Error on getting bookings set: " + e.getMessage());
        }

    }

    public static void deleteBooking(Booking booking) {
        String sqlQuery = "DELETE FROM bookings WHERE id=" + booking.getId();
        try {
            Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
            if (code != 1) {
                throw new SQLException("Something went wrong on deleting booking!");
            }
        } catch (SQLException e) {
            System.out.println("Error on getting booking: " + e.getMessage());
        }
    }

    private static TreeSet<Booking> getAllBookingsFromSqlQuery(String sqlQuery) {
        TreeSet<Booking> allBookings = new TreeSet<>();
        try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery)) {
            while (results.next()) {
                allBookings.add(new Booking()
                        .setId(results.getLong("id"))
                        .setRoom(results.getLong("room"))
                        .setFromdate(results.getTimestamp("fromdate"))
                        .setTodate(results.getTimestamp("todate"))
                        .setVenue(results.getLong("venue")));
            }
        } catch (SQLException e) {
            System.out.println("Error on getting bookings set: " + e.getMessage());
        }
        return allBookings;

    }
}