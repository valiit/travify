package ee.travify.backend.resources;

import ee.travify.backend.dao.Hotel;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;
import java.util.TreeSet;

public class HotelResource {

    public static Set<Hotel> getAllHotels() {
        return getHotelsFromSqlQuery("SELECT * FROM hotels");
    }

    public static Hotel getHotelById(long hotelId) {
        return getHotelsFromSqlQuery("SELECT * FROM hotels WHERE id=" + hotelId).first();
    }

    private static TreeSet<Hotel> getHotelsFromSqlQuery(String sqlQuery){
        TreeSet<Hotel> allHotels = new TreeSet<>();
        try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery)) {
            while (results.next()) {
                Hotel hotel = new Hotel()
                        .setId(results.getLong("id"))
                        .setCity(CityResource.getCityById(results.getLong("in_city")))
                        .setName(results.getString("name"));
                allHotels.add(hotel);
            }
        } catch (SQLException e) {
            System.out.println("Error on getting hotels set: " + e.getMessage());
        }
        return allHotels;
    }
}
