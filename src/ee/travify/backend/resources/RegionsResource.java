package ee.travify.backend.resources;

import ee.travify.backend.dao.Region;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;
import java.util.TreeSet;

public class RegionsResource {
    public static Set<Region> getAllRegions() {
        return getRegionsFromSqlQuery("SELECT * FROM regions");
    }

    public static Region getRegionsById(long regionId) {
        return getRegionsFromSqlQuery("SELECT * FROM regions WHERE id = " + regionId).first();
    }

    private static TreeSet<Region> getRegionsFromSqlQuery(String sqlQuery) {
        TreeSet<Region> regions = new TreeSet<>();
        try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery)) {
            while (results.next()) {
                regions.add(new Region().setId(results.getLong("id"))
                        .setRegion_name(results.getString("region_name")));
            }
        } catch (SQLException e) {
            System.out.println("Error on getting regions set: " + e.getMessage());
        }
        return regions;
    }
}




