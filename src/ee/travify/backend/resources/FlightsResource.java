package ee.travify.backend.resources;

import ee.travify.backend.dao.Airport;
import ee.travify.backend.dao.Flight;

import java.sql.CallableStatement;
import java.sql.*;
import java.time.Instant;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

public class FlightsResource {
    public static Flight getFlightById(long flightId) {
        if (flightId != 0) {
            return getFlightsFromSqlQuery("SELECT * FROM allflightsdata WHERE id = " + flightId).first();
        }
        else return null;
    }

    public static Set<Flight> getFlightsFromToBetweenDates(Airport fromAirport, Airport toAirport,
                                                           String fromTimeStamp, String toTimeStamp) {
        if (Objects.equals(fromTimeStamp, "") || fromTimeStamp == null) {
            fromTimeStamp = String.valueOf(new Timestamp(java.util.Date.from(Instant.now()).getTime()));

        }

        String sqlQuery = "SELECT * FROM allflightsdata";
        if (fromTimeStamp != null) {
            sqlQuery += " WHERE departure_time > TIMESTAMP('" + fromTimeStamp + "')"; 
            if (toTimeStamp != null) {
                sqlQuery += " AND departure_time < TIMESTAMP('" + toTimeStamp + "')";
            }
        }
        return getFlightsFromSqlQuery(sqlQuery);
    }

    public static String saveFlight(Set<Flight> flights) {
        String sqlQuery = "INSERT INTO flights (from_airport, to_airport, departure_time, arrival_time, seats_total, seats_free, price) VALUES ";

        for (Flight flight : flights) {
            sqlQuery += String.format("(%s, %s, '%s', '%s', %s, %s, %s),",
                    flight.getFrom_airport().getId(),
                    flight.getTo_airport().getId(),
                    new java.sql.Timestamp(flight.getDeparture_time().getTime()),
                    new java.sql.Timestamp(flight.getArrival_time().getTime()),
                    flight.getSeats_total(),
                    flight.getSeats_free(),
                    flight.getPrice());
        }
        sqlQuery = sqlQuery.substring(0, sqlQuery.length() - 1);

        try (Statement stmt = DatabaseConnection.getConnection().createStatement()) {
            stmt.executeUpdate(sqlQuery);
        } catch (SQLException e) {
            System.out.println("Error on saving flight: " + e.getMessage());
            return "ERROR";
        }
        return "OK";
    }

    public static TreeSet<Flight> getAllFlightsFromToCityByDepartureBetweenDates(Long fromCityId, Long toCityId,
                                                                             String earliestFlightDepartureTime,
                                                                             String latestFlightDepartureTime,int maxNumberOfFlights) {

        if (Objects.equals(earliestFlightDepartureTime, "") || earliestFlightDepartureTime == null) {
            earliestFlightDepartureTime = String.valueOf(new Timestamp(java.util.Date.from(Instant.now()).getTime()));
        }

        String sqlQuery = "SELECT * FROM allflightsdata";
        if (fromCityId != null) {
            sqlQuery += " WHERE from_city_id= " + fromCityId;
            if (toCityId != null) {
                sqlQuery += " AND to_city_id= " + toCityId;
            }
            if (earliestFlightDepartureTime != null) {
                sqlQuery += " AND departure_time > TIMESTAMP('" + earliestFlightDepartureTime + "')";
            }
            if (latestFlightDepartureTime != null) {
                sqlQuery += " AND departure_time < TIMESTAMP('" + latestFlightDepartureTime + "')";
            }

            if (maxNumberOfFlights !=0) {
                sqlQuery += " ORDER BY price ASC LIMIT "+maxNumberOfFlights;
            }

        }

        return getFlightsFromSqlQuery(sqlQuery);
    }

    public static TreeSet<Flight> getAllFlightsByFromToCityAndByArrival(Long toCityId, Long fromCityId,
                                                                    String earliestFlightArrivalTime, String latestFlightArrivalTime, int maxNumberOfFlights) {

        String sqlQuery = "SELECT * FROM allflightsdata";
        if (toCityId != null) {
            sqlQuery += " WHERE to_city_id= " + toCityId;
            if (fromCityId != null) {
                sqlQuery += " AND from_city_id= " + fromCityId;
            }
            if (latestFlightArrivalTime != null) {
                sqlQuery += " AND arrival_time < TIMESTAMP('" + latestFlightArrivalTime  + "')";
            }

            if (earliestFlightArrivalTime != null) {
                sqlQuery += " AND arrival_time > TIMESTAMP('" + earliestFlightArrivalTime + "')";
            }

            if (maxNumberOfFlights !=0) {
                sqlQuery += " ORDER BY price ASC LIMIT "+maxNumberOfFlights;
            }
        }
        System.out.println(sqlQuery);
        return getFlightsFromSqlQuery(sqlQuery);
    }



	public static Set<Flight> getAvailableFlightsFromToCityByArrivalFlex (int fromCityID, int toCityID, int arrivalFlex, String startDate){
		TreeSet<Flight> allFlights = new TreeSet<>();
	try {
		CallableStatement storedProc = DatabaseConnection.getConnection().prepareCall("call getAvailableFlightsFromToCityByArrivalFlex (?,?,?,?)");
		storedProc.setInt(1, fromCityID);
		storedProc.setInt(2, toCityID);
		storedProc.setInt(3, arrivalFlex);
		storedProc.setString(4, startDate);
		boolean hadResults = storedProc.execute();

		while(hadResults) {
			ResultSet results = storedProc.getResultSet();
			while (results.next()) {
				Flight flight = new Flight()
						.setArrival_time(results.getDate("arrival_time"))
						.setDeparture_time(results.getDate("departure_time"))
						.setFrom_airport(AirportResource.getAirportById(results.getLong("from_airport")))
						.setTo_airport(AirportResource.getAirportById(results.getLong("to_airport")))
						.setPrice(results.getBigDecimal("price"))
						.setSeats_free(results.getLong("seats_free"));
				allFlights.add(flight);
			}
			results.close();
			hadResults = storedProc.getMoreResults();
			}
	} catch (SQLException e) {
		System.out.println("Error on getting flight data: " + e.getMessage());
	}

	return allFlights;

	}



    private static TreeSet<Flight> getFlightsFromSqlQuery(String sqlQuery) {
        TreeSet<Flight> flights = new TreeSet<>();
        System.out.println(sqlQuery);
        try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery)) {
            while (results.next()) {
                flights.add(new Flight()
                        .setArrival_time(results.getDate("arrival_time"))
                        .setDeparture_time(results.getDate("departure_time"))
                        .setFrom_airport(AirportResource.getAirportById(results.getLong("from_airport_id")))
                        .setTo_airport(AirportResource.getAirportById(results.getLong("to_airport_id")))
                        .setCityFromName(CityResource.getCityById(results.getLong("from_city_id")))
                        .setCityToName(CityResource.getCityById(results.getLong("to_city_id")))
                        .setSeats_total(results.getLong("seats_total"))
                        .setSeats_free(results.getLong("seats_free"))
                        .setPrice(results.getBigDecimal("price"))
                        .setId(results.getLong("id")));
            }
        } catch (SQLException e) {
            System.out.println("Error on getting flights: " + e.getMessage());
        }
        return flights;

    }
}

