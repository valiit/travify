package ee.travify.backend.resources;

import ee.travify.backend.dao.Country;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;
import java.util.TreeSet;

public class CountryResource {

    public static Set<Country> getAllCountries() {
        return getAllCountriesFromSqlQuery("SELECT * FROM countries");
    }

    public static Country getCountryById(Long countryId) {
        return  getAllCountriesFromSqlQuery("Select * from countries where id=" + countryId).first();
    }

    private static TreeSet<Country> getAllCountriesFromSqlQuery(String sqlQuery) {
        TreeSet<Country> allCountries = new TreeSet<>();
        try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery)) {
            while (results.next()) {
                allCountries.add(new Country()
                        .setId(results.getLong("id"))
                        .setName(results.getString("name")));
            }
        } catch (SQLException e) {
            System.out.println("Error on getting countries set: " + e.getMessage());
        }
        return allCountries;
    }

}
