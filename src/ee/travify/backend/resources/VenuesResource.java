package ee.travify.backend.resources;

import ee.travify.backend.dao.Venue;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;
import java.util.TreeSet;

public class VenuesResource {
    public static Set<Venue> getAllVenues() {
        return getVenuesFromSqlQuery("SELECT * FROM venues");
    }

    public static Venue getVenuesById(long venueId) {
        if (venueId!=0) {
            return getVenuesFromSqlQuery("SELECT * FROM venues WHERE id= " + venueId).first();
        } else {
            return null;
        }

    }

    public static TreeSet<Venue> getVenuesInRegion(long regionId) {
        if (regionId != 0) {
            return getVenuesFromSqlQuery ("select DISTINCT venues.*, cities.in_region from venues" +
                    "  JOIN cities on cities.id = venues.city" +
                    "  JOIN regions on cities.in_region = " + regionId);
        } else {
            return null;
        }
    }

    private static TreeSet<Venue> getVenuesFromSqlQuery(String sqlQuery) {
        TreeSet<Venue> venues = new TreeSet<>();
        try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery)) {//siit algab try catchi body loogeliste sulgede vahel
            while (results.next()) {
                venues.add(new Venue().setId(results.getLong("id"))
                        .setCity(CityResource.getCityById(results.getLong("city")))
                        .setName(results.getString("name"))
                        .setFacilities(results.getString("facilities"))
                        .setPlaces(results.getLong("places"))
                        .setPrice(results.getBigDecimal("price")));
            }
        } catch (SQLException e) {
            System.out.println("Error on getting venues set: " + e.getMessage());
        }
        return venues;
    }

}
