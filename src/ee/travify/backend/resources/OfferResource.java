package ee.travify.backend.resources;

import ee.travify.backend.dao.*;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class OfferResource {

	public static Offer constructOffer(Long eventId, long regionId) {

		if (eventId != 0) {
			Map<Long, BigDecimal> perVenueBudgets = new HashMap<Long, BigDecimal>();

			Event currentEvent = EventResource.getEventById(eventId);
			TreeSet<Venue> allVenues = VenuesResource.getVenuesInRegion(regionId);

			for (Venue oneVenue : allVenues) {
				BigDecimal thisVenueBudget = getBudgetForVenue(currentEvent, oneVenue);
				System.out.println(oneVenue);
				perVenueBudgets.put(oneVenue.getId(), thisVenueBudget);
				perVenueBudgets = sortByValue(perVenueBudgets);

			}

			System.out.println(perVenueBudgets);

			Offer newOffer = new Offer();
			currentEvent.setBudget((BigDecimal) perVenueBudgets.values().toArray()[0]);

			long cheapestVenueId = (long) perVenueBudgets.keySet().toArray()[0];

			for (Iterator<Venue> it = allVenues.iterator(); it.hasNext();) {
				Venue f = it.next();
				if (f.getId().equals(cheapestVenueId))
					currentEvent.setVenue(f);
			}

			// System.out.println(perVenueBudgets.get());

			// currentEvent.setVenue(allVenues.)
			newOffer.setEvent(currentEvent);
			return newOffer;
		}
		return null;
	}

	private static BigDecimal getBudgetForVenue(Event currentEvent, Venue oneVenue) {
		BigDecimal thisVenueBudget = oneVenue.getPrice();
		Date arrivalToEventDate = currentEvent.getFromDate(); // TODO: siia peaks arvestama ka evendi alguses AIRPORT >
																// VENUE transpordi aja
		Date departureToStartPointDate = currentEvent.getToDate(); // TODO: siia peaks arvestama ka evendi lõpus VENUE >
																	// AIRPORT transpordi aja

		for (Participant oneParticipant : currentEvent.getParticipants()) {
			thisVenueBudget = getBudgetForParticipant(currentEvent, oneVenue, thisVenueBudget, arrivalToEventDate,
					departureToStartPointDate, oneParticipant);
		}
		return thisVenueBudget;
	}

	private static BigDecimal getBudgetForParticipant(Event currentEvent, Venue oneVenue, BigDecimal thisVenueBudget,
			Date arrivalToEventDate, Date departureToStartPointDate, Participant oneParticipant) {
		thisVenueBudget = getArrivalFlightBudget(oneVenue, thisVenueBudget, arrivalToEventDate, oneParticipant);

		thisVenueBudget = getDepartureFlightBudget(oneVenue, thisVenueBudget, departureToStartPointDate,
				oneParticipant);
		thisVenueBudget = getHotelRoomBudget(currentEvent, oneVenue, thisVenueBudget, oneParticipant);
		return thisVenueBudget;
	}

	private static BigDecimal getArrivalFlightBudget(Venue oneVenue, BigDecimal thisVenueBudget,
			Date arrivalToEventDate, Participant oneParticipant) {
		TreeSet<Flight> arrivalToEventFlights = FlightsResource.getAllFlightsByFromToCityAndByArrival(
				oneVenue.getCity().getId(),
				oneParticipant.getStartLocation().getId(),
				arrivalToEventDate.toString(),
				null, 1);

		if (arrivalToEventFlights != null && arrivalToEventFlights.size() > 0) {
			oneParticipant.setArrivalFlightBooking(arrivalToEventFlights.first());// TODO: hiljem võiks saada anda
																					// participandile külge panna
																					// rohkem kui 1 flighti, mille
																					// hulgast valida
			thisVenueBudget = thisVenueBudget.add(oneParticipant.getArrivalFlightBooking().getPrice());

		}
		return thisVenueBudget;
	}

	private static BigDecimal getDepartureFlightBudget(Venue oneVenue, BigDecimal thisVenueBudget,
			Date departureToStartPointDate, Participant oneParticipant) {
		TreeSet<Flight> departureToStarPointFlights = FlightsResource
				.getAllFlightsFromToCityByDepartureBetweenDates(
						oneVenue.getCity().getId(),
						oneParticipant.getStartLocation().getId(),
						departureToStartPointDate.toString(),
						null, // TODO: see on põmst departureToStartPointDate + Flex
						1);
		if (departureToStarPointFlights != null && departureToStarPointFlights.size() > 0) {
			oneParticipant.setDepartureFlightBooking(departureToStarPointFlights.first());// TODO: hiljem võiks
																							// saada anda
																							// participandile külge
																							// panna rohkem kui 1
																							// flighti, mille
																							// hulgast valida
			thisVenueBudget = thisVenueBudget.add(oneParticipant.getDepartureFlightBooking().getPrice());
		}
		return thisVenueBudget;
	}

	private static BigDecimal getHotelRoomBudget(Event currentEvent, Venue oneVenue, BigDecimal thisVenueBudget,
			Participant oneParticipant) {
		TreeSet<Hotelroom> hotelrooms = HotelroomResource.getAvailableInBetween(
				oneVenue.getCity().getId(),
				oneParticipant.getArrivalFlightBooking().getArrival_time().toString(),
				oneParticipant.getDepartureFlightBooking().getDeparture_time().toString());

		if (hotelrooms != null && hotelrooms.size() > 0) {// TODO: hiljem võiks saada anda participandile külge
			// panna rohkem kui 1 hotelroomi, mille hulgast valida
			oneParticipant.setHotelbooking(getUniqueRoomForParticipant(hotelrooms, currentEvent));
			thisVenueBudget = thisVenueBudget.add(oneParticipant.getHotelbooking().getPrice());
		}
		return thisVenueBudget;
	}

	private static Hotelroom getUniqueRoomForParticipant(TreeSet<Hotelroom> hotelrooms, Event currentEvent) {
		for (Hotelroom hotelRoomForParticipant : hotelrooms) {
			boolean roomBooked = isRoomAssignedForOtherPartisipant(currentEvent.getParticipants(),
					hotelRoomForParticipant);
			if (!roomBooked) {
				return hotelRoomForParticipant;
			}
		}
		return null;
	}

	private static boolean isRoomAssignedForOtherPartisipant(TreeSet<Participant> participants,
			Hotelroom firstRoomInSet) {
		for (Participant eventParticipant : participants) {
			if (eventParticipant.getHotelbooking() != null
					&& eventParticipant.getHotelbooking().getId().equals(firstRoomInSet.getId())) {
				return true;
			}
		}
		return false;
	}

	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
		return map.entrySet()
				.stream()
				.sorted(Map.Entry.comparingByValue(/* Collections.reverseOrder() */))
				.collect(Collectors.toMap(
						Map.Entry::getKey,
						Map.Entry::getValue,
						(e1, e2) -> e1,
						LinkedHashMap::new));
	}
}
