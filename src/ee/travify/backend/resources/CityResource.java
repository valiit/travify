package ee.travify.backend.resources;

import ee.travify.backend.dao.City;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.TreeSet;


public class CityResource {

    public static TreeSet<City> getAllCities() {
        return getAllCitiesFromSqlQuery("SELECT * FROM cities");
    }


    public static TreeSet<City> getCityByCountryId(long countryId) {
        return getAllCitiesFromSqlQuery("SELECT * FROM cities WHERE in_country=" + countryId);
    }

    public static City getCityById(Long cityId) {
        if (cityId != 0) {
            return getAllCitiesFromSqlQuery("SELECT * FROM cities WHERE id=" + cityId).first();
        } else {
            return null;
        }
    }

    private static TreeSet<City> getAllCitiesFromSqlQuery(String sqlQuery) {
        TreeSet<City> allCities = new TreeSet<>();

        try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery)) {
            while (results.next()) {
                City city = new City()
                        .setId(results.getLong("id"))
                        .setIn_country(CountryResource.getCountryById(results.getLong("in_country")))
                        .setIn_region(RegionsResource.getRegionsById(results.getLong("in_region")))
                        .setName(results.getString("name"));
                allCities.add(city);
            }

        } catch (SQLException e) {
            System.out.println("Error on getting cities set: " + e.getMessage());
        }
        return allCities;

    }
}
