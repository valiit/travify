package ee.travify.backend.dao;

import org.apache.commons.lang.builder.CompareToBuilder;

import java.math.BigDecimal;

public class Hotelroom implements Comparable<Hotelroom> {
	private Long id;
	private String room_name;
	private Long places;
	private BigDecimal price;
	private Hotel hotel;

	public Hotel getHotel() {
		return hotel;
	}

	public Hotelroom setHotel(Hotel hotel) {
		this.hotel = hotel;
		return this;
	}

	public Long getId() {
		return id;
	}

	public Hotelroom setId(Long id) {
		this.id = id;
		return this;
	}

	public String getRoom_name() {
		return room_name;
	}

	public Hotelroom setRoom_name(String room_name) {
		this.room_name = room_name;
		return this;
	}

	public Long getPlaces() {
		return places;
	}

	public Hotelroom setPlaces(Long places) {
		this.places = places;
		return this;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public Hotelroom setPrice(BigDecimal price) {
		this.price = price;
		return this;
	}

	@Override
	public int compareTo(Hotelroom o) {
		return new CompareToBuilder()
				.append(this.getPrice(), o.getPrice())
				.append(this.getPlaces(), o.getPlaces())
				.append(this.getRoom_name(), o.getRoom_name())
				.toComparison();
	}

	@Override
	public String toString() {
		return "HotelRooom:[name=" + room_name + "]";
	}
}
