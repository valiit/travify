package ee.travify.backend.dao;

import org.apache.commons.lang.builder.CompareToBuilder;

import java.math.BigDecimal;

public class Venue implements Comparable<Venue> {
    private Long id;
    private City city;
    private String name;
    private String facilities;
    private Long places;
    private BigDecimal price;

    public Long getId() {
        return id;
    }

    public Venue setId(Long id) {
        this.id = id;
        return this;
    }

    public City getCity() {
        return city;
    }

    public Venue setCity(City city) {
        this.city = city;
        return this;
    }

    public String getName() {
        return name;
    }

    public Venue setName(String name) {
        this.name = name;
        return this;
    }

    public String getFacilities() {
        return facilities;
    }

    public Venue setFacilities(String facilities) {
        this.facilities = facilities;
        return this;
    }

    public Long getPlaces() {
        return places;
    }

    public Venue setPlaces(Long places) {
        this.places = places;
        return this;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Venue setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    @Override
    public int compareTo(Venue o) {
        return new CompareToBuilder()
                .append(this.getPrice(), o.getPrice())
                .append(this.getCity(), o.getCity())
                .toComparison();
    }
}