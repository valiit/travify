package ee.travify.backend.dao;

import org.apache.commons.lang.builder.CompareToBuilder;

import java.math.BigDecimal;
import java.util.Date;
import java.util.TreeSet;

public class Event implements Comparable<Event>{
    private Long id;
    private Long organizer;
    private BigDecimal budget;
    private String title;
    private Venue venue;
    private Date fromDate;
    private Date toDate;
    private Integer arrivalFlex;
    private Integer departureFlex;
    private TreeSet<Participant> participants;
    private Region region;

    public Region getRegion() {
        return region;
    }

    public Event setRegion(Region region) {
        this.region = region;
        return this;
    }

    public BigDecimal getBudget() {
        return budget;
    }

    public Event setBudget(BigDecimal budget) {
        this.budget = budget;
        return this;
    }

    public Integer getArrivalFlex() {
		return arrivalFlex;
	}

	public Event setArrivalFlex(Integer arrivalFlex) {
		this.arrivalFlex = arrivalFlex;
		return this;
	}

	public Integer getDepartureFlex() {
		return departureFlex;
	}
    public Event setDepartureFlex(Integer departureFlex) {
		this.departureFlex = departureFlex;
		return this;
	}

    public TreeSet<Participant> getParticipants() {
        return participants;
    }

    public Event setParticipants(TreeSet<Participant> participants) {
        this.participants = participants;
        return this;
    }

    public Event addParticipant(Participant oneParticipant) {
        this.participants.add(oneParticipant);
        return this;
    }

    public Long getOrganizer() {
        return organizer;
    }

    public Event setOrganizer(Long organizer) {
        this.organizer = organizer;
        return this;
    }

    public Long getId() {
        return id;
    }

    public Event setId(Long id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Event setTitle(String title) {
        this.title = title;
        return this;
    }

    public Venue getVenue() {
        return venue;
    }

    public Event setVenue(Venue venue) {
        this.venue = venue;
        return this;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public Event setFromDate(Date fromDate) {
        this.fromDate = fromDate;
        return this;
    }

    public Date getToDate() {
        return toDate;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", venue=" + venue +
                ", fromDate=" + fromDate +
                ", toDate=" + toDate +
                '}';
    }

    public Event setToDate(Date toDate) {
        this.toDate = toDate;
        return this;
    }

    @Override
    public int compareTo(Event o) {
        return new CompareToBuilder()
                .append(this.getFromDate(), o.getFromDate())
                .append(this.getTitle(), o.getTitle())
                .append(this.getToDate(), o.getToDate())
                .toComparison();

    }
}
