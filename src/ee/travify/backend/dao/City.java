package ee.travify.backend.dao;

import org.apache.commons.lang.builder.CompareToBuilder;

public class City implements Comparable<City>{
    private Long id;
    private String name;
    private Country in_country;
    private Region in_region;

    public Long getId() {
        return id;
    }

    public City setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public City setName(String name) {
        this.name = name;
        return this;
    }

    public Country getIn_country() {
        return in_country;
    }

    public City setIn_country(Country in_country) {
        this.in_country = in_country;
        return this;
    }

    public Region getIn_region() {
        return in_region;
    }

    public City setIn_region(Region in_region) {
        this.in_region = in_region;
        return this;
    }

    @Override
    public int compareTo(City o) {
        return new CompareToBuilder()
                .append(this.getName(), o.getName())
                .append(this.getIn_country().getName(), o.getIn_country().getName())
                .toComparison();
    }
}
