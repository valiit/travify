package ee.travify.backend.dao;

import org.apache.commons.lang.builder.CompareToBuilder;

public class User implements Comparable<User>{
    private Long id;
    private String username;
    private String password;
    private String email;
    private City def_location;
    private String first_name;
    private String last_name;

    public Long getId() {
        return id;
    }

    public User setId(Long id) {
        this.id = id;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public User setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public City getDef_location() { 
    	return def_location; }

    public User setDef_location(City def_location) {
        this.def_location = def_location;
        return this;
    }

    public String getFirst_name() {
        return first_name;
    }

    public User setFirst_name(String first_name) {
        this.first_name = first_name;
        return this;
    }

    public String getLast_name() {
        return last_name;
    }

    public User setLast_name(String last_name) {
        this.last_name = last_name;
        return this;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", def_location=" + def_location +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                '}';
    }

    @Override
    public int compareTo(User o) {
        return new CompareToBuilder()
                .append(this.getLast_name(), o.getLast_name())
                .append(this.getFirst_name(), o.getFirst_name())
                .append(this.getEmail(), o.getEmail())
                .toComparison();
    }
}
