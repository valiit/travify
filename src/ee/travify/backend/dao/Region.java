package ee.travify.backend.dao;

import org.apache.commons.lang.builder.CompareToBuilder;

public class Region implements Comparable<Region> {
    private Long id;
    private String region_name;

    public Long getId() {
        return id;
    }

    public Region setId(Long id) {
        this.id = id;
        return this;
    }

    public String getRegion_name() {
        return region_name;
    }

    public Region setRegion_name(String region_name) {
        this.region_name = region_name;
        return this;
    }

    @Override
    public int compareTo(Region o) {
        return new CompareToBuilder()
                .append(this.getRegion_name(), o.getRegion_name())
                .toComparison();
    }
}
