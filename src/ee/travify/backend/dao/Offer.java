package ee.travify.backend.dao;


import java.util.TreeSet;

public class Offer {
    private Event event;
    private TreeSet<Participant> participants;

    public TreeSet<Participant> getParticipants() {
        return participants;
    }

    public Offer setParticipants(TreeSet<Participant> participants) {
        this.participants = participants;
        return this;
    }

    public Event getEvent() {
        return event;
    }

    public Offer setEvent(Event event) {
        this.event = event;
        return this;
    }
}

