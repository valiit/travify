package ee.travify.backend.dao;

import org.apache.commons.lang.builder.CompareToBuilder;

import java.math.BigDecimal;
import java.util.Date;

public class Flight implements Comparable<Flight> {
    private Long id;
    private Airport from_airport;
    private Airport to_airport;
    private Date departure_time;
    private Date arrival_time;
    private Long seats_total;
    private Long seats_free;
    private BigDecimal price;
    private City cityFromName;
    private City cityToName;

    public Long getId() {
        return id;
    }

    public Flight setId(Long id) {
        this.id = id;
        return this;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "id=" + id +
                ", from_airport=" + from_airport +
                ", to_airport=" + to_airport +
                ", departure_time=" + departure_time +
                ", arrival_time=" + arrival_time +
                ", seats_total=" + seats_total +
                ", seats_free=" + seats_free +
                ", price=" + price +
                '}';
    }

    public Airport getFrom_airport() {
        return from_airport;
    }

    public Flight setFrom_airport(Airport from_airport) {
        this.from_airport = from_airport;
        return this;
    }

    public Airport getTo_airport() {
        return to_airport;
    }

    public Flight setTo_airport(Airport to_airport) {
        this.to_airport = to_airport;
        return this;
    }

    public Date getDeparture_time() {
        return departure_time;
    }

  /*public Flight setDeparture_time(java.sql.Timestamp departure_time) {
    this.departure_time = departure_time;
    return this;
  }*/

    public Flight setDeparture_time(Date departure_time) {
        //SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-DD HH:mm:ss");
        //DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-DD HH:mm:ss");
        //DateTime dt = formatter.parseDateTime(departure_time);
        //formatter.
        this.departure_time = departure_time;
        //System.out.println("DEBUG: "+this.departure_time);
        return this;
    }

    public Date getArrival_time() {
        return arrival_time;
    }

    public Flight setArrival_time(Date arrival_time) {
        this.arrival_time = arrival_time;
        return this;
    }

    public Long getSeats_total() {
        return seats_total;
    }

    public Flight setSeats_total(Long seats_total) {
        this.seats_total = seats_total;
        return this;
    }

    public Long getSeats_free() {
        return seats_free;
    }

    public Flight setSeats_free(Long seats_free) {
        this.seats_free = seats_free;
        return this;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Flight setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public City getCityFromName() {
        return cityFromName;
    }

    public Flight setCityFromName(City cityFromName) {
        this.cityFromName = cityFromName;
        return this;
    }

    public City getCityToName() {
        return cityToName;
    }

    public Flight setCityToName(City cityToName) {
        this.cityToName = cityToName;
        return this;
    }

    @Override
    public int compareTo(Flight o) {
        return new CompareToBuilder()
                .append(this.getDeparture_time(), o.getDeparture_time())
                .append(this.getArrival_time(), o.getArrival_time())
                .append(this.getFrom_airport(), o.getFrom_airport())
                .append(this.getTo_airport(), o.getTo_airport())
                .toComparison();
    }
}
