package ee.travify.backend.dao;

import org.apache.commons.lang.builder.CompareToBuilder;

public class Hotel implements Comparable<Hotel>{
    private Long id;
    private String name;
    private City city;

    public Long getId() {
        return id;
    }

    public Hotel setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Hotel setName(String name) {
        this.name = name;
        return this;
    }

    public City getCity() {
        return city;
    }

    public Hotel setCity(City city) {
        this.city = city;
        return this;
    }

    @Override
    public int compareTo(Hotel o) {
        return new CompareToBuilder()
                .append(this.getName(), o.getName())
                .toComparison();
    }
}
