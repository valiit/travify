package ee.travify.backend.dao;

import org.apache.commons.lang.builder.CompareToBuilder;

public class Airport implements Comparable<Airport>{
    private Long id;
    private String airport_code;
    private Country country;
    private Region in_region;
    private City in_city;

    public Long getId() {
        return id;
    }

    public Airport setId(Long id) {
        this.id = id;
        return this;
    }

    public String getAirport_code() {
        return airport_code;
    }

    public Airport setAirport_code(String airport_code) {
        this.airport_code = airport_code;
        return this;
    }

    @Override
    public String toString() {
        return "Airport{" +
                "id=" + id +
                ", airport_code='" + airport_code + '\'' +
                ", country=" + country +
                ", in_region=" + in_region +
                ", in_city=" + in_city +
                '}';
    }

    public Country getCountry() {
        return country;
    }

    public Airport setCountry(Country country) {
        this.country = country;
        return this;
    }

    public Region getIn_region() {
        return in_region;
    }

    public Airport setIn_region(Region in_region) {
        this.in_region = in_region;
        return this;
    }

    public City getIn_city() {
        return in_city;
    }

    public Airport setIn_city(City in_city) {
        this.in_city = in_city;
        return this;
    }


    @Override
    public int compareTo(Airport o) {
        return new CompareToBuilder()
                .append(this.getAirport_code(), o.getAirport_code())
                .append(this.getIn_city().getName(), o.getIn_city().getName())
                .append(this.getCountry().getName(), o.getCountry().getName())
                .toComparison();
    }
}
