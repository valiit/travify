package ee.travify.backend.dao;

import org.apache.commons.lang.builder.CompareToBuilder;

public class Country implements Comparable<Country>{
    private Long id;
    private String name;

    public Long getId() {
        return id;
    }

    public Country setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Country setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public int compareTo(Country o) {
        return new CompareToBuilder()
                .append(this.getName(), o.getName())
                .toComparison();
    }
}
