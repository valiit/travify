package ee.travify.backend.dao;

import org.apache.commons.lang.builder.CompareToBuilder;

public class Participant implements Comparable<Participant> {
    private Long id;
    private User user;
    private long eventId;
    private Hotelroom hotelbooking;
    private Flight arrivalFlightBooking;
    private Flight departureFlightBooking;
    private City startLocation;

    public City getStartLocation() {
        return startLocation;
    }

    public Participant setStartLocation(City startLocation) {
        this.startLocation = startLocation;
        return this;
    }

    public Flight getDepartureFlightBooking() {
        return departureFlightBooking;
    }

    public Participant setDepartureFlightBooking(Flight departureFlightBooking) {
        this.departureFlightBooking = departureFlightBooking;
        return this;
    }

    public Flight getArrivalFlightBooking() {
        return arrivalFlightBooking;
    }

    public Participant setArrivalFlightBooking(Flight arrivalFlightBooking) {
        this.arrivalFlightBooking = arrivalFlightBooking;
        return this;
    }

    public Long getId() {
        return id;
    }

    public Participant setId(Long id) {
        this.id = id;
        return this;
    }

    public User getUser() {
        return user;
    }

    public Participant setUser(User user) {
        this.user = user;
        return this;
    }

    public long getEventId() {
        return eventId;
    }

    public Participant setEventId(long eventId) {
        this.eventId = eventId;
        return this;
    }

    public Hotelroom getHotelbooking() {
        return hotelbooking;
    }

    public Participant setHotelbooking(Hotelroom hotelbooking) {
        this.hotelbooking = hotelbooking;
        return this;
    }
    @Override
    public String toString() {
        return "Participant{" +
                "id=" + id +
                ", user='" + user + '\'' +
                ", eventId='" + eventId + '\'' +
                ", hotelbooking=" + hotelbooking +
                ", arrivalFlightBooking='" + arrivalFlightBooking + '\'' +
                ", departureFlightBooking='" + departureFlightBooking + '\'' +
                ", startLocation='" + startLocation + '\'' +
                '}';
    }
    
    
    @Override
    public int compareTo(Participant o) {
        return new CompareToBuilder()
                .append(this.getUser().getLast_name(), o.getUser().getLast_name())
                .append(this.getUser().getFirst_name(), o.getUser().getFirst_name())
                .append(this.getUser().getEmail(), o.getUser().getEmail())
                .toComparison();
    }
}
