package ee.travify.backend.dao;

import org.apache.commons.lang.builder.CompareToBuilder;

public class Booking implements Comparable<Booking> {
    private Long id;
    private Long room;
    private java.sql.Timestamp fromdate;
    private java.sql.Timestamp todate;
    private Long venue;

    public Long getId() {
        return id;
    }

    public Booking setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getRoom() {
        return room;
    }

    public Booking setRoom(Long room) {
        this.room = room;
        return this;
    }

    public java.sql.Timestamp getFromdate() {
        return fromdate;
    }

    public Booking setFromdate(java.sql.Timestamp fromdate) {
        this.fromdate = fromdate;
        return this;
    }

    public java.sql.Timestamp getTodate() {
        return todate;
    }

    public Booking setTodate(java.sql.Timestamp todate) {
        this.todate = todate;
        return this;
    }

    public Long getVenue() {
        return venue;
    }

    public Booking setVenue(Long venue) {
        this.venue = venue;
        return this;
    }


    @Override
    public int compareTo(Booking bookingToCompare) {
        return new CompareToBuilder()
                .append(this.getFromdate(), bookingToCompare.getFromdate())
                .append(this.getTodate(), bookingToCompare.getTodate())
                .toComparison();
    }

}
