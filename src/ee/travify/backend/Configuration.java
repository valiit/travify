package ee.travify.backend;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Configuration {
    public String databaseUser;
    public String databasePassword;
    public String databaseUrl;

    public Configuration() {
        Properties prop = new Properties();
        InputStream input = null;

        try {
            input = Thread.currentThread().getContextClassLoader().getResourceAsStream(
                    "travify.properties");
            // load a properties file
            prop.load(input);
            // get the property value and print it out
            databaseUrl = prop.getProperty("dburl");
            databaseUser = prop.getProperty("dbuser");
            databasePassword = prop.getProperty("dbpassword");

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
