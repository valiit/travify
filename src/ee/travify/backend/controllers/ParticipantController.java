package ee.travify.backend.controllers;


import ee.travify.backend.dao.Participant;
import ee.travify.backend.resources.ParticipantResource;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Set;
import java.util.TreeSet;

@Path("/participants")
public class ParticipantController {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Participant> getAllParticipants() {
        return ParticipantResource.getAllParticipants();
    }

    @GET
    @Path("/{participantId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Participant getParticipantById(@PathParam("participantId") long participantId) {
        return ParticipantResource.getParticipantById(participantId);
    }

    @POST
    //@Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public boolean addParticipant(Participant participant) {
        return ParticipantResource.addParticipant(participant);
    }
    

   @POST
   @Path("/addmultipleparticipants")
  // @Produces(MediaType.APPLICATION_JSON)
   @Consumes(MediaType.APPLICATION_JSON)
   public boolean addMultipleParticipants(Set<Participant> multipleParticipants) {
	   return ParticipantResource.addMultipleParticipants(multipleParticipants);
   }

   
   
   
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateParticipant(Participant participant) {
        ParticipantResource.updateParticipant(participant);
    }

    @DELETE
    @Path("/del/{participantId}")
    // @Consumes(MediaType.APPLICATION_JSON)
    public void deleteParticipant(@PathParam("participantId") long participantId) {
        ParticipantResource.deleteParticipant(participantId);
    }

}
