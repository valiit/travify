package ee.travify.backend.controllers;

import ee.travify.backend.dao.Event;
import ee.travify.backend.resources.EventResource;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Set;

@Path("/events")
public class EventsController extends BaseController {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Event> getAllEvents() {

        if (checkIfUserLoggedIn() && authenticatedUser != null) {
            return EventResource.getAllEvents(authenticatedUser);
        } else {
            return null;
        }
    }

    @GET
    @Path("/{eventId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Event getEventById(@PathParam("eventId") long eventId) {
        return EventResource.getEventById(eventId);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Event addNewEvent(Event event) {
        System.out.println(event);
        if (checkIfUserLoggedIn() && authenticatedUser != null) {
            event.setOrganizer(authenticatedUser.getId());
            return EventResource.addNewEvent(event);
        } else {
            return null;
        }
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateEvent(Event event) {
        EventResource.updateEvent(event);
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteEvent(Event event) {
        EventResource.deleteEvent(event);
    }


}
