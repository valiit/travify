package ee.travify.backend.controllers;

import ee.travify.backend.dao.Country;
import ee.travify.backend.resources.CountryResource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Set;

@Path("/countries")
public class CountriesController {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Country> getAllAirports() {
        return CountryResource.getAllCountries();
    }

    @GET
    @Path("/{countryId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Country getCountriesById(@PathParam("countryId") Long countryId) {
        return CountryResource.getCountryById(countryId);
    }

}
