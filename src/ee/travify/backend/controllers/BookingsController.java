package ee.travify.backend.controllers;

import ee.travify.backend.dao.Booking;
import ee.travify.backend.resources.BookingResource;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Set;


@Path("/bookings")
public class BookingsController {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Booking> getAllBookings() {
        return BookingResource.getAllBookings();
    }

    @GET
    @Path("/{bookingId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Booking getBookingById(@PathParam("bookingId") Long bookingId) {
        return BookingResource.getBookingById(bookingId);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Booking addNewBooking(Booking booking) {
        return BookingResource.addNewBooking(booking);
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateBooking(Booking booking) {
        BookingResource.updateBooking(booking);
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteBooking(Booking booking) {
        BookingResource.deleteBooking(booking);
    }

}
