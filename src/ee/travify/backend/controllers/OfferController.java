package ee.travify.backend.controllers;

import ee.travify.backend.dao.Offer;
import ee.travify.backend.resources.OfferResource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/offers")

public class OfferController {

    @GET
    @Path("/{eventId}/{regionId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Offer getOffer(@PathParam("eventId") long eventId, @PathParam("regionId") long regionId) {
        return OfferResource.constructOffer(eventId,regionId);
    }

}
