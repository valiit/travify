package ee.travify.backend.controllers;

import ee.travify.backend.dao.Airport;
import ee.travify.backend.resources.AirportResource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Set;


@Path("/airports")
public class AirportsController {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Airport> getAllAirports() {
        return AirportResource.getAllAirports();
    }

    @GET
    @Path("/{airportId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Airport getAirportsById(@PathParam("airportId") Long airportId) {
        return AirportResource.getAirportById(airportId);
    }
    
    @GET
    @Path("/byCity/{cityId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Airport> getAirportsByCityId(@PathParam("cityId") Long cityId) {
        return AirportResource.getAirportsByCityId(cityId);
    }
}
