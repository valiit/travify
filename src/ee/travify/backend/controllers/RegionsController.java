package ee.travify.backend.controllers;


import ee.travify.backend.dao.Region;
import ee.travify.backend.resources.RegionsResource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Set;


@Path("/regions")
public class RegionsController {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Region> getAllRegions() {
        return RegionsResource.getAllRegions();
    }

    @GET
    @Path("/{regionId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Region getRegionsById(@PathParam("regionId") int regionId) {
        return RegionsResource.getRegionsById(regionId);
    }
}

