package ee.travify.backend.controllers;

import ee.travify.backend.dao.Hotelroom;
import ee.travify.backend.resources.HotelroomResource;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Set;

@Path("/hotelrooms")
public class HotelroomsController {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Hotelroom> getAllHotelrooms() {
        return HotelroomResource.getAllHotelrooms();
    }

    @GET
    @Path("/{hotelroomId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Hotelroom getHotelroomById(@PathParam("hotelroomId") long hotelroomId) {
        return HotelroomResource.getHotelroomById(hotelroomId);
    }

    @GET
    @Path("/getAvailableInBetween/{cityId}/{fromDate}/{toDate}")
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Hotelroom> getAvailableInBetween(@PathParam("cityId") int cityId, @PathParam("fromDate") String fromDate, @PathParam("toDate") String toDate) {
        return HotelroomResource.getAvailableInBetween(cityId,fromDate,toDate);
    }


    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String saveHotelRoom(Set<Hotelroom> rooms) {

        return HotelroomResource.saveHotelRoom(rooms);
    }


}

