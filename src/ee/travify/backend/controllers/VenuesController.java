package ee.travify.backend.controllers;

import ee.travify.backend.dao.Venue;
import ee.travify.backend.resources.VenuesResource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Set;

@Path("/venues")
public class VenuesController {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Venue> getAllVenues() {
        return VenuesResource.getAllVenues();
    }

    @GET
    @Path("/{venueId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Venue getVenuesById(@PathParam("venueId") int venueId) {
        return VenuesResource.getVenuesById(venueId);
    }
}