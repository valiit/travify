package ee.travify.backend.controllers;

import ee.travify.backend.dao.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;


public class BaseController {
    public User authenticatedUser = null;

    @Context
    private HttpServletRequest request;

    @GET
    @Path("/checkIfLoggedIn")
    public boolean checkIfUserLoggedIn() {
        boolean retVal = false;
        HttpSession session = request.getSession (true);
        authenticatedUser = (User) session.getAttribute("user");

        if (authenticatedUser!=null) {
            retVal = true;
        } else {
            System.out.println("ERRROOOOOR: unauthenticated uuuseeerrr");
        }
        return retVal;
    }

    @GET
    @Path("/logOut")
    public boolean logOut() {
        HttpSession session = request.getSession (true);
        session.setAttribute("user",null);
        authenticatedUser = null;
        return true;
    }
}
