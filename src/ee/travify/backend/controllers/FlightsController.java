package ee.travify.backend.controllers;

import ee.travify.backend.dao.Flight;
import ee.travify.backend.dao.Hotelroom;
import ee.travify.backend.resources.FlightsResource;
import ee.travify.backend.resources.HotelroomResource;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Set;

@Path("/flights")
public class FlightsController {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Flight> getFlightsFromToBetweenDates(@QueryParam("fromTimeStamp") String fromTimeStamp,
                                                    @QueryParam("toTimeStamp") String toTimeStamp) {
        return FlightsResource.getFlightsFromToBetweenDates(null, null, fromTimeStamp, toTimeStamp);
    }


    @GET
    @Path("/fromto/{fromCityId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Flight> getAllFlightsFromToCityByDeparture(@PathParam("fromCityId") Long fromCityId,
                                               @QueryParam("toCityId") Long toCityId,
                                               @QueryParam("fromTimeStamp") String fromTimeStamp,
                                               @QueryParam("toTimeStamp") String toTimeStamp,
                                            @QueryParam("maxNumberOfFlights") int maxNumberOfFlights) {
        return FlightsResource.getAllFlightsFromToCityByDepartureBetweenDates(fromCityId, toCityId, 
        																fromTimeStamp, toTimeStamp,maxNumberOfFlights);
    }
    
    @GET
    @Path("/arrivals/{toCityId}/{fromCityId}")
    @Produces(MediaType.APPLICATION_JSON)
    //TODO: nimi peaks kajastama et lennud on sorteeritud hinna järgi ja neid saab maxNumberiga limiteerida
    public Set<Flight> getAllFlightsFromToCityByArrival(@PathParam("toCityId") Long toCityId,
    											@PathParam("fromCityId") Long fromCityId,    																			
    											@QueryParam("flightArrivalTime") String flightArrivalTime,
                                                @QueryParam("flightDepartureTime") String flightDepartureTime,
                                                @QueryParam("maxNumberOfFlights") int maxNumberOfFlights)
    											{
    	return FlightsResource.getAllFlightsByFromToCityAndByArrival(toCityId, fromCityId,
                flightArrivalTime, flightDepartureTime, maxNumberOfFlights);
    }
    
    @GET
    @Path("/getFlightsInBetween/{fromCityID}/{toCityID}/{arrivalFlex}/{startDate}")
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Flight> getAvailableFlightsFromToCityByArrivalFlex(@PathParam("fromCityID") int fromCityID, @PathParam("toCityID") int toCityID, 
    		@PathParam("arrivalFlex") int arrivalFlex, @PathParam("startDate") String startDate) {
        return FlightsResource.getAvailableFlightsFromToCityByArrivalFlex(fromCityID,toCityID,arrivalFlex,startDate);
    }

    @PUT
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    public String saveFlight(Set<Flight> flightData) {
        return FlightsResource.saveFlight(flightData);
    }
}

