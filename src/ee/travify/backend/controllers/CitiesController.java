package ee.travify.backend.controllers;

import ee.travify.backend.dao.City;
import ee.travify.backend.resources.CityResource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Set;


@Path("/cities")
public class CitiesController {


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Set<City> getAllCities() {
        return CityResource.getAllCities();
    }

    @GET
    @Path("/{cityId}")
    @Produces(MediaType.APPLICATION_JSON)
    public City getCityById(@PathParam("cityId") Long cityId) {
        return CityResource.getCityById(cityId);
    }

    @GET
    @Path("/inCountry/{countryId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Set<City> getCityByCountryId(@PathParam("countryId") Long countryId) {
        return
                CityResource.getCityByCountryId(countryId);

    }

}
