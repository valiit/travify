package ee.travify.backend.controllers;

import ee.travify.backend.dao.Hotel;
import ee.travify.backend.resources.HotelResource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Set;

@Path("/hotels")
public class HotelsController {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Hotel> getAllHotels() {
        return HotelResource.getAllHotels();
    }

    @GET
    @Path("/{hotelId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Hotel getHotelById(@PathParam("hotelId") long hotelId) {
        return HotelResource.getHotelById(hotelId);
    }

}
