package ee.travify.backend.controllers;

import ee.travify.backend.dao.User;
import ee.travify.backend.resources.UserResource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.Set;

@Path("/users")
public class UserController extends BaseController{
    @Context
    private HttpServletRequest request;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Set<User> getAllUsers() {
        return UserResource.getAllUsers();
    }



    @GET
    @Path("/{userId}")
    @Produces(MediaType.APPLICATION_JSON)
    public User getUserById(@PathParam("userId") long userId) {
        return UserResource.getUserById(userId);
    }

	@POST
	@Path("/checkIfUserExists")
	@Consumes(MediaType.APPLICATION_JSON)
	public Boolean checkIfUserExists(User user) {
		return UserResource.checkIfUserExists(user);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public User addNewUser(User user) {
		return UserResource.addNewUser(user);
	}

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateUser(User user) {
        UserResource.updateUser(user);
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteUser(User user) {
        UserResource.deleteUser(user);
    }


    @POST
    @Path("/login")
    //@Produces(MediaType.APPLICATION_JSON)
    //@Consumes(MediaType.APPLICATION_JSON)
    public boolean userLogin(@FormParam("userName") String userName,
                          @FormParam("password") String password) {
        System.out.println(userName);
        System.out.println(password);
        boolean retVal = false;
        User authenticatedUser = UserResource.userLogin(userName,password);
        if (authenticatedUser != null) {
            HttpSession session = request.getSession(true);
            session.setAttribute("user", authenticatedUser);
            retVal = true;
        }
        return retVal;
    }


}
