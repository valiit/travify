-- ---
-- Globals
-- ---

-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
-- SET FOREIGN_KEY_CHECKS=0;

-- ---
-- Table 'Users'
--
-- ---

DROP DATABASE IF EXISTS travify;
DROP DATABASE IF EXISTS Travify;
CREATE DATABASE travify;
USE travify;


DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id`           INTEGER      NOT NULL AUTO_INCREMENT,
  `username`     VARCHAR(10)  NOT NULL,
  `password`     VARCHAR(255) NOT NULL,
  `email`        VARCHAR(255) NOT NULL,
  `def_location` INTEGER      NULL,
  `first_name`   VARCHAR(50)  NOT NULL,
  `last_name`    VARCHAR(50)  NOT NULL DEFAULT ' ',
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'Regions'
--
-- ---

DROP TABLE IF EXISTS `regions`;

CREATE TABLE `regions` (
  `id`          INTEGER      NOT NULL AUTO_INCREMENT,
  `region_name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'Flights'
--
-- ---

DROP TABLE IF EXISTS `flights`;

CREATE TABLE `flights` (
  `id`             INTEGER   NOT NULL AUTO_INCREMENT,
  `from_airport`   INTEGER   NOT NULL,
  `to_airport`     INTEGER   NOT NULL,
  `departure_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `arrival_time`   TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `seats_total`    INTEGER   NOT NULL,
  `seats_free`     INTEGER   NOT NULL,
  `price`          DECIMAL(15,6)   NOT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'Airports'
--
-- ---

DROP TABLE IF EXISTS `airports`;

CREATE TABLE `airports` (
  `id`           INTEGER     NOT NULL AUTO_INCREMENT,
  `airport_code` VARCHAR(10) NOT NULL,
  `country`      INTEGER     NOT NULL,
  `in_region`    INTEGER     NOT NULL,
  `in_city`      INTEGER     NOT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'Countries'
--
-- ---

DROP TABLE IF EXISTS `countries`;

CREATE TABLE `countries` (
  `id`   INTEGER      NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'Hotels'
--
-- ---

DROP TABLE IF EXISTS `hotels`;

CREATE TABLE `hotels` (
  `id`      INTEGER      NOT NULL AUTO_INCREMENT,
  `name`    VARCHAR(255) NOT NULL,
  `in_city` INTEGER      NOT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'Cities'
--
-- ---

DROP TABLE IF EXISTS `cities`;

CREATE TABLE `cities` (
  `id`         INTEGER      NOT NULL AUTO_INCREMENT,
  `name`       VARCHAR(255) NOT NULL,
  `in_country` INTEGER      NOT NULL,
  `in_region`  INTEGER      NOT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'HotelRooms'
--
-- ---

DROP TABLE IF EXISTS `hotelrooms`;

CREATE TABLE `hotelrooms` (
  `id`        INTEGER      NOT NULL AUTO_INCREMENT,
  `room_name` VARCHAR(255) NOT NULL,
  `places`    INTEGER      NOT NULL,
  `hotel`     INTEGER      NOT NULL,
  `price`     DECIMAL(25,6)       NOT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'Bookings'
--
-- ---

DROP TABLE IF EXISTS `bookings`;

CREATE TABLE `bookings` (
  `id`       INTEGER  NOT NULL AUTO_INCREMENT,
  `room`     INTEGER  NULL,
  `fromDate` DATETIME NOT NULL,
  `toDate`   DATETIME NOT NULL,
  `venue`    INTEGER  NULL,
  `flight`   INTEGER  NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'Venues'
--
-- ---

DROP TABLE IF EXISTS `venues`;

CREATE TABLE `venues` (
  `id`         INTEGER      NOT NULL AUTO_INCREMENT,
  `city`       INTEGER      NOT NULL,
  `name`       VARCHAR(255) NOT NULL,
  `facilities` MEDIUMTEXT   NOT NULL
  COMMENT 'what facilities (ie. dataprojector, all seats have computers',
  `places`     INTEGER      NOT NULL,
  `price`      DECIMAL(15,6)      NOT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'participants'
--
-- ---

DROP TABLE IF EXISTS `participants`;

CREATE TABLE `participants` (
  `id`            INTEGER NOT NULL AUTO_INCREMENT,
  `userId`        INTEGER NOT NULL,
  `eventId`       INTEGER NOT NULL,
  `hotelbooking`  INTEGER NULL,
  `arrivalFlightBooking` INTEGER NULL,
  `departureFlightBooking` INTEGER NULL,
  `startLocation` INTEGER NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'events'
--
-- ---

DROP TABLE IF EXISTS `events`;

CREATE TABLE `events` (
  `id`    INTEGER      NOT NULL AUTO_INCREMENT,
  `organizer`    INTEGER      NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `venue` INTEGER       NULL,
  `region` INTEGER       NOT NULL,
  `budget`      DECIMAL(15,6)      NULL,
  `fromDate`  DATETIME     NOT NULL,
  `toDate`    DATETIME     NOT NULL,
  `arrivalFlex` INTEGER NULL,
  `departureFlex` INTEGER NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Foreign Keys
-- ---

ALTER TABLE `users`
  ADD FOREIGN KEY (def_location) REFERENCES `cities` (`id`);
ALTER TABLE `flights`
  ADD FOREIGN KEY (from_airport) REFERENCES `airports` (`id`);
ALTER TABLE `flights`
  ADD FOREIGN KEY (to_airport) REFERENCES `airports` (`id`);
ALTER TABLE `airports`
  ADD FOREIGN KEY (country) REFERENCES `countries` (`id`);
ALTER TABLE `airports`
  ADD FOREIGN KEY (in_region) REFERENCES `regions` (`id`);
ALTER TABLE `airports`
  ADD FOREIGN KEY (in_city) REFERENCES `cities` (`id`);
ALTER TABLE `hotels`
  ADD FOREIGN KEY (in_city) REFERENCES `cities` (`id`);
ALTER TABLE `cities`
  ADD FOREIGN KEY (in_country) REFERENCES `countries` (`id`);
ALTER TABLE `cities`
  ADD FOREIGN KEY (in_region) REFERENCES `regions` (`id`);
ALTER TABLE `hotelrooms`
  ADD FOREIGN KEY (hotel) REFERENCES `hotels` (`id`);
ALTER TABLE `bookings`
  ADD FOREIGN KEY (room) REFERENCES `hotelrooms` (`id`);
ALTER TABLE `bookings`
  ADD FOREIGN KEY (venue) REFERENCES `venues` (`id`);
ALTER TABLE `bookings`
  ADD FOREIGN KEY (flight) REFERENCES `flights` (`id`);
ALTER TABLE `venues`
  ADD FOREIGN KEY (city) REFERENCES `cities` (`id`);
ALTER TABLE `participants`
  ADD FOREIGN KEY (userId) REFERENCES `users` (`id`);
ALTER TABLE `participants`
  ADD FOREIGN KEY (eventId) REFERENCES `events` (`id`);
ALTER TABLE `participants`
  ADD FOREIGN KEY (hotelbooking) REFERENCES `bookings` (`id`);
ALTER TABLE `participants`
  ADD FOREIGN KEY (arrivalFlightBooking) REFERENCES `bookings` (`id`);
ALTER TABLE `participants`
  ADD FOREIGN KEY (departureFlightBooking) REFERENCES `bookings` (`id`);
ALTER TABLE `participants`
  ADD FOREIGN KEY (startLocation) REFERENCES `cities` (`id`);
ALTER TABLE `events`
  ADD FOREIGN KEY (venue) REFERENCES `bookings` (`id`);
ALTER TABLE `events`
  ADD FOREIGN KEY (organizer) REFERENCES `users` (`id`);
ALTER TABLE `events`
  ADD FOREIGN KEY (region) REFERENCES `regions` (`id`);


-- ---
-- Table Properties
-- ---

ALTER TABLE `users`
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;
ALTER TABLE `regions`
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;
ALTER TABLE `flights`
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;
ALTER TABLE `airports`
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;
ALTER TABLE `countries`
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;
ALTER TABLE `hotels`
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;
ALTER TABLE `cities`
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;
ALTER TABLE `hotelrooms`
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;
ALTER TABLE `bookings`
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;
ALTER TABLE `venues`
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;
ALTER TABLE `participants`
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;
ALTER TABLE `events`
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;

-- ---
-- Views
-- ---

DROP VIEW IF EXISTS allflightsdata;
CREATE VIEW allflightsdata AS
  SELECT
    f.id,
    f.arrival_time,
    f.departure_time,
    f.price,
    f.seats_free,
    f.seats_total,
    a_from.airport_code AS from_airport_code,
    a_from.id           AS from_airport_id,
    a_to.airport_code   AS to_airport_code,
    a_to.id             AS to_airport_id,
    c_from.name         AS from_city,
    c_from.id           AS from_city_id,
    c_to.name           AS to_city,
    c_to.id             AS to_city_id
  FROM flights AS f
    JOIN airports AS a_from ON f.from_airport = a_from.id
    JOIN airports AS a_to ON f.to_airport = a_to.id
    JOIN cities AS c_from ON a_from.in_city = c_from.id
    JOIN cities AS c_to ON a_to.in_city = c_to.id;


DROP VIEW IF EXISTS allhotelsdata;
CREATE VIEW allhotelsdata AS
  SELECT
    h.id,
    h.name,
    ci.name        AS city,
    co.name        AS country,
    re.region_name AS region
  FROM hotels AS h
    JOIN cities AS ci ON h.in_city = ci.id
    JOIN countries AS co ON ci.in_country = co.id
    JOIN regions AS re ON ci.in_region = re.id;

DROP VIEW IF EXISTS allairportsdata;
CREATE VIEW allairportsdata AS
  SELECT
    a.id,
    a.airport_code,
    ci.name     AS city,
    co.name     AS country,
    region_name AS regions
  FROM
    airports AS a
    JOIN
    cities AS ci ON ci.id = a.in_city
    JOIN
    countries AS co ON ci.in_country = co.id
    JOIN
    regions ON ci.in_region = regions.id;


DROP VIEW IF EXISTS allcitiesdata;
CREATE VIEW allcitiesdata AS

  SELECT
    ci.name     AS city,
    co.name     AS country,
    region_name AS regions
  FROM
    cities AS ci
    JOIN
    countries AS co ON ci.in_country = co.id
    JOIN
    regions ON ci.in_region = regions.id;


DROP VIEW IF EXISTS allbookingsdata;
CREATE VIEW allbookingsdata AS
  SELECT
    b.id,
    h.name       AS hotel,
    hs.room_name AS room,
    v.name       AS venue,
    b.fromDate,
    b.toDate
  FROM bookings AS b
    JOIN hotelrooms AS hs ON b.room = hs.id
    JOIN hotels AS h ON hs.hotel = h.id
    JOIN venues AS v ON b.venue = v.id;


DROP VIEW IF EXISTS alleventsdata;
CREATE VIEW alleventsdata AS
  SELECT
    events.title   AS eventTitle,
    events.id      AS eventId,
    events.toDate      AS toDate,
    events.fromDate    AS fromDate,
    venues.name    AS venuesName,
    c.name         AS cityName,
    countries.name AS countryName
  FROM
    events
    JOIN
    venues ON events.venue = venues.id
    JOIN
    cities AS c ON c.id = venues.city
    JOIN
    countries ON countries.id = c.in_country;


DROP VIEW IF EXISTS allhotelroomsdata;
CREATE VIEW allhotelroomsdata AS

  SELECT
    hotelrooms.id AS room_id,
    hotelrooms.room_name,
    hotelrooms.places,
    hotelrooms.price,
    hotels.name   AS hotel_name,
    hotels.id     AS hotel_id,
    hotels.in_city AS city_id
  FROM
    hotelrooms
    JOIN
    hotels ON hotelrooms.hotel = hotels.id
    JOIN
    cities ON hotels.in_city = cities.id;



/*DROP VIEW IF EXISTS participantData;
CREATE VIEW allParticipantData AS
  SELECT
    participants.id AS participant_id,
    users.first_name,
    users.last_name,
    users.email,
    users.id        AS user_id,
    events.title,
    events.id       AS event_id,
    cities.name     AS city,
    cities.id       AS city_id,
    countries.name  AS country,
    countries.id    AS country_id
  FROM participants
    JOIN users ON participants.userId = users.id
    JOIN events ON participants.eventId = events.id
    JOIN cities ON users.def_location = cities.id
    JOIN countries ON cities.in_country = countries.id;
*/

DROP VIEW IF EXISTS allhotelroombookings;
CREATE VIEW allhotelroombookings AS
  SELECT *
  FROM bookings
  WHERE room IS NOT NULL;

DROP VIEW IF EXISTS allflightbookings;
CREATE VIEW allflightbookings AS
  SELECT *
  FROM bookings
  WHERE flight IS NOT NULL;

DROP VIEW IF EXISTS allvenuebookings;
CREATE VIEW allvenuebookings AS
  SELECT *
  FROM bookings
  WHERE venue IS NOT NULL;


DROP PROCEDURE IF EXISTS get_available_rooms_in_city_in_between_dates;

delimiter //

CREATE PROCEDURE get_available_rooms_in_city_in_between_dates(cityID INT, startDateVar VARCHAR(20), endDateVar VARCHAR(20))
  BEGIN
    DECLARE startDate DATETIME;
    DECLARE endDate DATETIME;

    SELECT STR_TO_DATE(startDateVar,"%Y-%m-%d") INTO startDate;
    SELECT STR_TO_DATE(endDateVar,"%Y-%m-%d") INTO endDate;

    SELECT * FROM allHotelroomsData WHERE city_id = cityID AND room_id NOT IN
                                                               (SELECT room
                                                                FROM allhotelroombookings as aB
                                                                where (aB.fromDate >= startDate and aB.toDate <= endDate)
                                                                      or(aB.fromDate<=startDate and aB.toDate <= endDate and aB.toDate>=startDate)
                                                                      or(aB.toDate>=endDate and aB.fromDate<=endDate and aB.fromDate>=startDate)
                                                                      or (aB.fromDate<=startDate and aB.toDate>=endDate))
                                                                ORDER BY price ASC limit 0, 10;
  END//

delimiter ;


DROP PROCEDURE IF EXISTS getAvailableFlightsFromToCityByArrivalFlex;

delimiter //
CREATE PROCEDURE getAvailableFlightsFromToCityByArrivalFlex(fromCityID INT, toCityID INT, arrivalFlexVar INT, startDateVar VARCHAR(20))
BEGIN

    DECLARE startDate DATETIME;

    SELECT str_to_date(startDateVar, "%Y-%m-%d") INTO startDate;

 SELECT * FROM flights WHERE
			(arrival_time BETWEEN DATE_SUB(startDate, INTERVAL arrivalFlexVar hour) AND startDate)
			AND from_airport IN
			(SELECT id FROM airports WHERE in_city = fromCityID)
			AND to_airport IN
			(SELECT id FROM airports WHERE in_city = toCityID)
		ORDER BY price ASC limit 1;

    
    END//

delimiter ;


    /*$("#loadOffer").click(function(){
        loadOffer();
    })*/

  /*call getAvailableFlightsFromToCityByArrivalFlex (4,1,72,8);*/

   /*SELECT * FROM allFlightsData WHERE (arrival_time BETWEEN DATE_SUB(fromEvent, INTERVAL arrivalFlexVar hour) AND fromEvent) AND from_city_id = fromCityID AND to_city_id = toCityID;/*

/*SELECT fromDate FROM events WHERE id=1 INTO @fromEvent;
select @fromEvent;*/