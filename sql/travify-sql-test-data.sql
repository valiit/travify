-- Test Data
-- ---
USE travify;

INSERT INTO `regions` (`region_name`) VALUES
  ('Päikeserannik'),
  ('Põhja-Aafrika'),
  ('Kariibid'),
  ('Polüneesia'),
  ('Baltimaad'),
  ('Vahemere saared'),
  ('Lääne-Euroopa'),
  ('Ladina-Ameerika'),
  ('Põhja-Euroopa'),
  ('Kesk-Euroopa'),
  ('Kaug-Aasia');

INSERT INTO `countries` (`name`) VALUES
  ('Eesti'),
  ('Itaalia'),
  ('Küprose Vabariik'),
  ('Bahaamad'),
  ('Tonga'),
  ('Ühendkuningriik'),
  ('Hispaania'),
  ('Maroko'),
  ('Peruu'),
  ('Norra'),
  ('Saksamaa'),
  ('Vietnam');

INSERT INTO `cities` (`name`, `in_country`, `in_region`) VALUES
  ('Tallinn', '1', '5'),
  ('Vacca', '2', '6'),
  ('Küpros', '3', '6'),
  ('Nassau', '4', '3'),
  ('Nukualofa', '5', '4'),
  ('London', 6, 7),
  ('Marbella',7,1),
  ('Rabat',8,2),
  ('Lima', 9, '8'),
  ('Oslo', 10, 9),
  ('München', 11,10),
  ('Hanoi', 12, 11);

INSERT INTO `users` (`username`, `password`, `email`, `def_location`, `first_name`, `last_name`) VALUES
  ('erik', PASSWORD('eriksuit'), 'erik@suit.ee', '4', 'Erik', 'Suit'),
  ('marit', PASSWORD('maritmarit'), 'marit@marit.ee', '1', 'Marit', 'Ei ütle');

INSERT INTO `airports` (`airport_code`, `country`, `in_region`, `in_city`) VALUES
  ('EETN', '1', '5', '1'),
  ('ITL', '2', '6', '2'),
  ('CYP', '3', '6', '3'),
  ('BHM', '4', '3', '4'),
  ('TNG', '5', '4', '5'),
  ('LCY', 6,7,6),
  ('LGW', 6,7,6),
  ('LHR', 6,7,6),
  ('QRL', 7,1,7),
  ('RBA',8,2,8),
  ('LIM',9,8,9),
  ('OSL',10,9,10),
  ('MUC',11,10,11),
  ('HAN',12,11,12);

  
  
/*INSERT INTO `flights` (`from_airport`,`to_airport`,`departure_time`,`arrival_time`,`seats_total`,`seats_free`,`price`) VALUES
('1','5','2017-01-19 03:14:07', '2017-01-19 06:19:55','180','10','250'),
('3','4','2017-11-20 08:50:55','2017-11-20 19:46:00','250','80','110');*/

INSERT INTO `hotels` (`name`, `in_city`) VALUES
  ('Äge hotell', '1'),
  ('Jube hotell', '3'),
  ('Tore hotell', '2'),
  ('Kole hotell', '4'),
  ('Vinge hotell', '5'),
  ('Mõttetu hotell', '1'),
  ('Väsitav hotell', '4'),
  ('Päikese hotell', '11'),
  ('Kuu hotell', '12'),
  ('Tähe hotell', '10'),
  ('Pluuto hotell', '9'),
  ('Marsi hotell', 6),
  ('Veenuse hotell', 7),
  ('Jupiteri hotell', 8),
  ('Saturni hotell', 7);


INSERT INTO `hotelrooms` (`room_name`, `places`, `hotel`, `price`) VALUES
  ('Veel ägedam tuba', '1', '1', 54321.1234),
  ('Jubedamast jubedam tuba', '1', '2', '80'),
  ('Tillu-lillu tuba', '2', '3', '55'),
  ('WebXML tuba', '4', '4', '88'),
  ('Kärbse tuba', '2', '5', '90'),
  ('Binary tuba', '3', '6', '20'),
  ('Tuutu tuba', '1', '7', '40'),
  ('Miki tuba', 3,8,56),
  ('Minni tuba', 4, 9,86),
  ('Donaldi tuba', 2,10, 99),
  ('Tiki-Taku tuba', 2, 11, 22),
  ('Nu Pagadi tuba', 1,12, 40),
  ('Aladini tuba',5,13,88),
  ('Tuhkatriinu tuba',1, 14,33),
  ('Lõvikuninga tuba', 4,15,44),
  ('Shreki tuba',8,11,111);

INSERT INTO `venues` (`city`, `name`, `facilities`, `places`, `price`) VALUES
  ('1', 'Tallinna Linnahall', ' Dataprojektor', '50', '150'),
  ('2', 'Vacca dzungel', 'Sipelgapesa', '1547', '99'),
  ('5', 'Nuku"duku"muku', 'Muku-muku arvutid igal laual', '5', '80'),
  ('3', 'Cypress', 'Olemas on WC!!', '99', '75'),
  ('4', 'Baha-haha-men', 'Dataprojektor; suured kõlarid igas ruumi nurgas; mikrofon', '666', '100'),
  ('5', 'Pastakate tuba', 'Igal laual oma pastakas, paber; puuduvad arvutid!', '10', '25'),
  ('1', 'Olde Hansa katusekorrus', 'Lauad; pingid; küünlad', '40', '30'),
  ('4', 'Haikala hall', 'Tasuta vesi; veekindel dataprojektor', '44', '44'),
  ('3', 'Dao hall', 'Dataprojektor; arvutid igal laual', '55', '55'),
  ('2', 'Vacca koobas', 'Sile kivisein joonistamiseks; tugev seljatagune toetamiseks', '90', '15'),
  ('9', 'Mäestiku vaade', 'rõdu', '10', '100'),
  ('10', 'Lumine nõlv', 'suusahoiuruum ja saun', '20', '60'),
  ('11', 'Brandeburgi väravate hall', 'suur laud', '50', '100'),
  ('6','Toweri vangla hall', 'käerauad',6, 300),
  ('7','Härjatrofeede ruum', 'arvuti',20,100),
  ('8','Merevaate saal','teler', 15, 70),
  ('12','Kommunistliku partei saal','lauad, arvuti', 100,100);

INSERT INTO `bookings` (`room`,`fromDate`,`toDate`) VALUES
  ('5','2017-11-20 00:00:00','2017-11-25 00:00:00'),
  ('3','2017-11-17 00:00:00','2017-11-19 00:00:00'),
  ('4','2017-11-20 00:00:00','2017-11-22 00:00:00'),
  ('2','2017-11-21 00:00:00','2017-11-23 00:00:00');


INSERT INTO travify.events (organizer, title, venue, fromDate, toDate, arrivalFlex, departureFlex, region) VALUES (1, 'seminar', 1, '2017-11-20 01:01:01', '2017-12-02 20:22:00', '48', '24', 5);

INSERT INTO travify.participants (userId, eventId, hotelbooking, arrivalFlightBooking, departureFlightBooking, startLocation)
VALUES (1, 1, 1, 1, 1, 1),
(2, 1, 1, 1, 1,1);







