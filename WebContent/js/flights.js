var airports = null;
var hotels = null;

var backendPath = 'http://localhost:8080/travify/rest'; // no slash (/) in the end!
$(document).ready(function () {
    $("#loadAirports").click(function () {
        console.log(loadAirports());
        putRandomTimesToArrivalAndDeparture();
    });
    $("#loadflights").click(function () {
        console.log(loadFlights());

    });
    $("#genHotelRooms").click(function () {
        var hotelroomCount = $("#roomsCount").val();
        loadHotels(function () {
            var hotelroomsJSON = "[";
            for (var i = 0; i < hotelroomCount; i++) {
                hotelroomsJSON += JSON.stringify(generateRandomHotelRoom()) + ",";
            }
            hotelroomsJSON = hotelroomsJSON.substr(0, hotelroomsJSON.length - 1);
            hotelroomsJSON += "]";

            saveHotelRoom(hotelroomsJSON);
        })
     })

    $("#genFlights").click(function () {
        var flightCount = $("#flightsCount").val();
        console.log("generating "+flightCount+" flights");
        loadAirports(function () {
            var flightsJSON = "[";
            for (var i = 0; i < flightCount; i++) {
                flightsJSON += JSON.stringify(generateRandomFlight())+',';
            }
            flightsJSON = flightsJSON.substr(0, flightsJSON.length-1);
            flightsJSON += ']';

            saveFlight(flightsJSON);
        })

    });

});


function generateRandomFlight() {

    var noOfAirports = airports.length;
    var departureTime = getRandomMysqlTimeStampInFuture();

    var arrivalTime = getRandomMysqlTimeStampInFuture(departureTime.getTime(),15);

    var totalSeats = Math.floor(Math.random() * 200)+2;

    var freeSeats = -1;
    while (freeSeats < 0) {
        freeSeats = Math.floor(totalSeats - Math.random() * 190);
    }


    var from_airport_random = randomNumberInBetween(1, noOfAirports);
    var to_airport_random = randomNumberInBetween(1, noOfAirports);
    while (to_airport_random === from_airport_random) {
        to_airport_random = randomNumberInBetween(1, noOfAirports)
    }

    var flight = {
        "from_airport":
            {"id": from_airport_random},
        "to_airport":
            {"id": to_airport_random},
        "arrival_time": arrivalTime.toMysqlFormat(),
        "departure_time": departureTime.toMysqlFormat(),
        "seats_total": totalSeats,
        "seats_free": freeSeats,
        "price": randomNumberInBetween(1, 999)
    };

    return flight;

}

function saveFlight(flight) {
    console.log("SAVE flight DONEEEE",Math.random());
    $.ajax({
        url: backendPath + "/flights/",
        method: "PUT",
        contentType: "application/json;charset=utf-8",
        data: flight
    })
}

function randomNumberInBetween(a, b, doNotRound) {
    var number = (Math.random() * b) + a;
    if (!doNotRound) {
        return Math.floor(number);
    } else {
        return number;
    }

}

function loadAirports(callback) {
    var fromAirportSelect = $("#fromAirport");
    var toAirportSelect = $("#toAirport");

    $.ajax({
        url: backendPath + "/airports"
    }).done(function (data) {
        airports = data;
        if (callback !== undefined) {
            callback(data)
        }
        fromAirportSelect.html('');
        $.each(data, function (i, airport) {
            var $option = $('<option>', {value: airport.id, text: airport.airport_code});
            fromAirportSelect.append($option);
        });
        toAirportSelect.html('');
        $.each(data, function (i, airport) {
            var $option = $('<option>', {value: airport.id, text: airport.airport_code});
            toAirportSelect.append($option);
        })

    })
}

function twoDigits(d) {
    if (0 <= d && d < 10) return "0" + d.toString();
    if (-10 < d && d < 0) return "-0" + (-1 * d).toString();
    return d.toString();
}

Date.prototype.toMysqlFormat = function () {
    return this.getUTCFullYear() + "-" + twoDigits(1 + this.getUTCMonth()) + "-" + twoDigits(this.getUTCDate()) + "T" + twoDigits(this.getUTCHours()) + ":" + twoDigits(this.getUTCMinutes()) + ":" + twoDigits(this.getUTCSeconds()) + "+02:00";
};

function getRandomMysqlTimeStampInFuture(date, maxHoursFromStart) {
    var oneHour = 1000 * 60 * 60;

    if (date === undefined) {
        date = Date.now();
    } else {
        var startTime = date;
    }

    if (maxHoursFromStart !== undefined) {
        while (date - startTime < oneHour / 2) {
            date += Math.random() * (oneHour * maxHoursFromStart);
        }
    } else {
        date += Math.random() * (Math.random() * 10000000000);
    }

    return new Date(date);
}

function putRandomTimesToArrivalAndDeparture() {

    var time = getRandomMysqlTimeStampInFuture();
    $("#departureTime").val(time.toMysqlFormat());
    time = getRandomMysqlTimeStampInFuture(time.getTime(),15);
    $("#arrivalTime").val(time.toMysqlFormat());

    var totalSeats = Math.floor(Math.random() * 200);
    $("#seatsTotal").val(totalSeats);

    var freeSeats = -1;
    while (freeSeats < 0) {
        freeSeats = Math.floor(totalSeats - Math.random() * 190);
    }

    $("#seatsFree").val(freeSeats);
}

function loadHotels(callback) {
    $.ajax({
        url: backendPath + "/hotels"
    }).done(function (data) {
        hotels = data;
        if (callback !== undefined) {
            callback(data)
        }
    })
}

function generateRandomHotelRoom() {
        return {
            "room_name": generateRandomName(),
            "places": randomNumberInBetween(1, 5),
            "price": randomNumberInBetween(10, 250, true),
            "hotel": {"id": hotels[randomNumberInBetween(0, hotels.length)].id}
        };
}

function generateRandomName() {

    var adjectives = ["adamant", "adroit", "amatory", "animistic", "antic", "arcadian", "baleful", "bellicose", "bilious", "boorish", "calamitous", "caustic", "cerulean", "comely", "concomitant", "contumacious", "corpulent", "crapulous", "defamatory", "didactic", "dilatory", "dowdy", "efficacious", "effulgent", "egregious", "endemic", "equanimous", "execrable", "fastidious", "feckless", "fecund", "friable", "fulsome", "garrulous", "guileless", "gustatory", "heuristic", "histrionic", "hubristic", "incendiary", "insidious", "insolent", "intransigent", "inveterate", "invidious", "irksome", "jejune", "jocular", "judicious", "lachrymose", "limpid", "loquacious", "luminous", "mannered", "mendacious", "meretricious", "minatory", "mordant", "munificent", "nefarious", "noxious", "obtuse", "parsimonious", "pendulous", "pernicious", "pervasive", "petulant", "platitudinous", "precipitate", "propitious", "puckish", "querulous", "quiescent", "rebarbative", "recalcitant", "redolent", "rhadamanthine", "risible", "ruminative", "sagacious", "salubrious", "sartorial", "sclerotic", "serpentine", "spasmodic", "strident", "taciturn", "tenacious", "tremulous", "trenchant", "turbulent", "turgid", "ubiquitous", "uxorious", "verdant", "voluble", "voracious", "wheedling", "withering", "zealous"];
    var nouns = ["ninja", "chair", "pancake", "statue", "unicorn", "rainbows", "laser", "senor", "bunny", "captain", "nibblets", "cupcake", "carrot", "gnomes", "glitter", "potato", "salad", "toejam", "curtains", "beets", "toilet", "exorcism", "stick figures", "mermaid eggs", "sea barnacles", "dragons", "jellybeans", "snakes", "dolls", "bushes", "cookies", "apples", "ice cream", "ukulele", "kazoo", "banjo", "opera singer", "circus", "trampoline", "carousel", "carnival", "locomotive", "hot air balloon", "praying mantis", "animator", "artisan", "artist", "colorist", "inker", "coppersmith", "director", "designer", "flatter", "stylist", "leadman", "limner", "make-up artist", "model", "musician", "penciller", "producer", "scenographer", "set decorator", "silversmith", "teacher", "auto mechanic", "beader", "bobbin boy", "clerk of the chapel", "filling station attendant", "foreman", "maintenance engineering", "mechanic", "miller", "moldmaker", "panel beater", "patternmaker", "plant operator", "plumber", "sawfiler", "shop foreman", "soaper", "stationary engineer", "wheelwright", "woodworkers"];

    function jsUcfirst(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    function randomEl(list) {
        var i = Math.floor(Math.random() * list.length);
        return list[i];
    }

    return jsUcfirst(randomEl(adjectives) + ' ' + randomEl(nouns) + " room");
}


function saveHotelRoom(room) {
    $.ajax({
        url: backendPath + "/hotelrooms/",
        method: "PUT",
        contentType: "application/json;charset=utf-8",
        data: room
    }).error(function (data) {
        console.error(data, room)
    }).done(function (data) {
        console.log("Room added: ", data)
    })
}


function loadFlights() {
    console.log("flights");
    $.ajax({
        url: backendPath + "/flights"
    }).done(function (data) {
        console.log(data);
        return data;

    })
}
