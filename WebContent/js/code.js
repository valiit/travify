var backendPath = 'http://localhost:8080/rest'; // no slash (/) in the end!

if (!window.jQuery) {
    console.error("ERROR: no jQuery loaded");
}

$(document).ready(function () {
    checkIfUserIsLoggedIn()
    $('[data-toggle="tooltip"]').tooltip()
});

function checkIfUserIsLoggedIn() {
    $.ajax({
            url: backendPath + '/users/checkIfLoggedIn'
        }
    ).done(function (data) {
        if (data != "true") {
            initLoginAndRegisterPage()
        } else {
            $("#navLogOut").click(function () {
                $.ajax(backendPath + "/users/logOut").done(function () {
                    initLoginAndRegisterPage()
                })
            })
            initEventsPage()
        }
    }).fail(function (data) {
        initLoginAndRegisterPage();
    })
}

function initLoginAndRegisterPage() {
    $("#loginBtn").click(function () {
        postLoginForm()
    });

    showPage("#loginPage");

    $("#registerLink").click(function () {
        showPage("#registerPage");
    })

    $("#loginLink").click(function () {
        showPage("#loginPage");
    })
}

function initEventsPage() {
    $("#participantForm").on("show.bs.modal", function (event) {

        clearAllFormValues("#participantForm");

        var button = $(event.relatedTarget) // Button that triggered the modal
        $('#eventIdToAddParticipantTo').val(button.data('eventid')) // Extract info from data-* attributes

        loadCitiesToStartPoint();
        loadUsersfromBackend();

    });

    $("#addNewEventForm").on("show.bs.modal", function () {
        clearAllFormValues("#addNewEventForm");
    });

    showPage("#eventsPage");

    initDatePickers();
    loadRegions();

    refreshEventsTable();

    $("#addNewEvent").click(function () {
        $("#eventStartForm").toggle();
    });

    $("#saveButton").click(eventSaveButtonClick);

    $("#submitParticipantInfo").click(addParticipantToEvent);

    $('#participantFirstname').change(function () {
        setUserDataChanged(1);
    })
    $('#participantLastname').change(function () {
        setUserDataChanged(1);
    })
    $('#participantEmail').change(function () {
        setUserDataChanged(1);
    })
}

function refreshEventsTable() {
    loadAllEvents(function () {
        setOfferButtonClick()
    });
}

function clearAllFormValues(formId) {
    $(formId).find("input").val('');
}

function addParticipantToEvent() {
    var userData = {
        "id": $('#existingUserId').val(),
        "first_name": $('#participantFirstname').val(),
        "last_name": $('#participantLastname').val(),
        "email": $('#participantEmail').val(),
        "def_location": {"id": $('#citiesSelect').val()}
    };

            var participant = {
                "user": {"id": userData.id},
                "eventId": $("#eventIdToAddParticipantTo").val(),
        "hotelbooking": {"id": ''},
        "arrivalFlightBooking": {"id": ''},
        "departureFlightBooking": {"id": ''},
        "startLocation": userData.def_location
            }


    if (userData.id !== "") {
        if (($("#userDataChanged").val() === "0")) {


            saveParticipantToEvent(participant, function () {
                clearAllFormValues("#participantForm")
                refreshEventsTable();
            })

        } else {
            console.log("User data came from DB and is changed");
            updateUser(userData, function () {
                clearAllFormValues("#participantForm")
                saveParticipantToEvent(participant, function () {
                    clearAllFormValues("#participantForm")
                    refreshEventsTable();
                })
            });

        }
    }
    else {
        console.log("completely new user");
        saveUser(userData, function (data) {
            participant.user.id = data.id;
            clearAllFormValues("#participantForm")
            saveParticipantToEvent(participant, function () {
                clearAllFormValues("#participantForm")
                refreshEventsTable();
            })
        });


    }
}

function saveUser(userData, callback) {
    $.ajax({
        url: backendPath + "/users",
        method: "POST",
        data: JSON.stringify(userData),
        contentType: "application/json;charset=utf-8"
    }).done(function (data) {
        loadUsersfromBackend()
        if (callback != undefined) {
            callback(data)
        }

    })
}

function updateUser(userData, callback) {
    $.ajax({
        url: backendPath + "/users",
        method: "PUT",
        data: JSON.stringify(userData),
        contentType: "application/json;charset=utf-8"
    }).done(function (data) {
        loadUsersfromBackend()
        if (callback != undefined) {
            callback(data)
        }

    })
}


function saveParticipantToEvent(participant, callback) {
    $.ajax({
        url: backendPath + "/participants",
        method: "POST",
        data: JSON.stringify(participant),
        contentType: "application/json;charset=utf-8"
    }).done(function (data) {
        if (callback != undefined) {
            callback(data)
        }
    })
}

function loadCitiesToStartPoint() {
    loadCities($("#participantForm").find("#citiesSelect"))
};

function AddAllUsersToList(userList) {
    $("#existingUsers").html("<option></option>");
    $.each(userList, function (i, user) {
        $("#existingUsers").append('<option value="' + user.id + '">' + user.last_name + ', ' + user.first_name + '</option>');
    });

    $("#existingUsers").off('change').change(function (event) {
        fillParticipantFormFromExistingUserData(userList, this.value)
    });
};

function setUserDataChanged(flag) {
    $("#userDataChanged").val(flag)
}

function fillParticipantFormFromExistingUserData(allUsers, selectedId) {
    var selectedUser = allUsers.filter(function (data) {
        return data.id == selectedId
    })
    $('#participantFirstname').val(selectedUser[0].first_name);
    $('#participantLastname').val(selectedUser[0].last_name);
    $('#participantEmail').val(selectedUser[0].email);
    $('#citiesSelect').val(selectedUser[0].def_location.id);
    $('#existingUserId').val(selectedUser[0].id);
    setUserDataChanged(0);
}

function setOfferButtonClick() {
    $(".getOfferButton").click(function () {
        var regionId = $(this).data('region');
        var eventId = $(this).data('eventid');
        initOffersPage();
        loadOffer(eventId, regionId);
    })
}

function initOffersPage() {
    $('#offerCancel').off("click").click(function () {
        showPage("#eventsPage");
    })
}

function eventSaveButtonClick() {
    var title = $("#eventName").val();
    var regionID = $("#regionSelect").val();
    var fromDate = $("#fromDate").val();
    var toDate = $("#toDate").val();
    var arrivalFlex = $("#arrivalFlex").val();
    var departureFlex = $("#departureFlex").val();
    if ((title !== "" && title !== undefined)
        && (regionID !== "" && regionID !== undefined)
        && (fromDate !== "" && fromDate !== undefined)
        && (toDate !== "" && toDate !== undefined)
        && (arrivalFlex !== "" && arrivalFlex !== undefined)
        && (departureFlex !== "" && departureFlex !== undefined)
        && (fromDate < toDate)) {
        saveEvent(title, regionID,
            fromDate, toDate,
            arrivalFlex, departureFlex)
    } else {
        $("#eventMsg")
            .text(
                "Can't save incorrect data...")
    }

}

function showPage(show, callback) {
    if ((show === "#eventsPage") || (show === "#offerPage")) {
        $(".navbar").removeClass("hidden");
    }
    $(".page").removeClass("visible").addClass("hidden");
    $(show).addClass("visible").removeClass("hidden").show();
    if (callback != undefined) {
        callback();
    }
}

function postLoginForm() {
    var loginData = {
        "userName": $("#loginUserName").val(),
        "password": $("#loginPassword").val()
    };
    $.post({
        url: backendPath + "/users/login",
        data: loginData
    }).done(function (data) {
        if (data === "true") {
            showPage("#eventsPage", function () {
                initEventsPage()
            });
        }
    })

}

function initDatePickers() {
    if ($(".dateTimePicker").length > 0) {
        jQuery.datetimepicker.setLocale('et');

        jQuery('#fromDate').datetimepicker({
            timepicker: true,
            format: 'Y-m-d H:i:00'
        });
        jQuery('#toDate').datetimepicker({
            timepicker: true,
            format: 'Y-m-d H:i:00'
        });
    }
}

function loadRegions() {
    $.ajax({
        url: backendPath + "/regions"
    }).done(function (data) {
        var $regionsSelect = $("#regionSelect").html('');
        $.each(data, function (i, itemData) {
            var $option = $('<option>', {
                value: itemData.id,
                text: itemData.region_name
            });
            $regionsSelect.append($option);
        });
        $regionsSelect.change();
    })

}

function loadCities($selectToFill) {
    var $citiesSelect;

    if ($selectToFill != undefined) {
        $citiesSelect = $selectToFill;
    } else {
        $citiesSelect = $("#citiesSelect");
    }

    $.ajax({
        url: backendPath + "/cities"
    }).done(function (data) {
        $citiesSelect.html('');
        $.each(data, function (i, itemData) {
            var $option = $('<option>', {
                value: itemData.id,
                text: itemData.name
            });
            $citiesSelect.append($option);
        });
        $citiesSelect.change();
    })

}

function loadUsersfromBackend() {
    $.ajax(backendPath + "/users/").done(function (data) {
        AddAllUsersToList(data)
    })
}

function saveEvent(title, regionID, fromDate, toDate, arrivalFlex, departureFlex) {

    /*ERIK! Ära kustuta ära seda välja kommitud koodi! ArrivalFlex = new Date (Date.parse(fromDate) - (arrivalFlex*24*60*60*1000))
    departureFlex = new Date (Date.parse(toDate) + (departureFlex*24*60*60*1000))*/

    var postData = {
        "title": title,
        "fromDate": fromDate.replace(' ', 'T'),
        "toDate": toDate.replace(' ', 'T'),
        "id": 0,
        "arrivalFlex": arrivalFlex * 24,
        "departureFlex": departureFlex * 24,
        "region": {"id": regionID}
    };

    $.ajax({
        url: backendPath + "/events",
        method: "POST",
        data: JSON.stringify(postData),
        contentType: "application/json;charset=utf-8"
    }).done(function () {
        $("#eventMsg").text("Event saved successfully")
        $("#eventStartForm").hide()
        loadAllEvents()
    }).fail(function () {
        $("#eventMsg").text("Event not save, ERROOOR!")
    })

}

function loadAllEvents(callback) {
    $.ajax({
        url: backendPath + "/events"
    }).done(function (data) {
        $('#eventsContainer').html(parseEvents(data))
        $(".participantDeleteButton").click(function(event){
            if (confirm("Do you want to delete this participant?")) {
                    deleteParticipant($(this).data("participantid")) }
        })
        if (callback != undefined) {
            callback();
        }
    })
}

function sprintf() {
    var input = arguments[0];
    for (var i = 1; i < arguments.length; i++) {
        var toReplace = new RegExp("\{([" + (i - 1) + "])\}", "g")
        input = input.replace(toReplace, arguments[i]);
    }
    return input;
}

function reformatDate(date) {
    return date.replace('T', '<br>');
}

function parseEvents(eventsDataJSON) {
    var eventsTableContent = "";
    var eventRowTemplate = '<tr id="eventRow" data-id="{0}">' +
        '<td>{1}</td>' +
        '<td>{2}</td>' +
        '<td>{3}</td>' +
        '<td>{4}</td>' +
        '<td>{5}</td>' +
        '<td><div class="btn-group" role="group">{6}{7}</div></td>' +
        '</tr>'

    $.each(eventsDataJSON, function (i, oneEvent) {
        eventsTableContent += sprintf(eventRowTemplate,
            oneEvent.id,
            oneEvent.title,
            reformatDate(oneEvent.fromDate),
            reformatDate(oneEvent.toDate),
            parseVenue(oneEvent.venue),
            parseParticipants(oneEvent.participants),
            addParticipantButton(oneEvent.fromDate, oneEvent.id),
            addGetOfferButton(oneEvent.fromDate, oneEvent.id, oneEvent.region.id)
        )
    });

    var eventsTableFrame = '<table class="table table-dark table-striped" id="eventsTable">' +
        '<thead>' +
        '<th>Title</th>' +
        '<th>From</th>' +
        '<th>To</th>' +
        '<th>Events</th>' +
        '<th>Participants</th>' +
        '<th></th>' +
        '</thead><tbody>{0}</tbody></table>';

    return sprintf(eventsTableFrame, eventsTableContent);
}

function addParticipantButton(eventStart, eventId) {
    var date = new Date(eventStart)
    var current = new Date

    if (current < date) {
        return '<button data-toggle="modal" data-eventId="' + eventId + '" data-target="#participantForm" id="addParticipantButton" class="btn btn-secondary">Add participant</button>'
    }
    else {
        return "event closed"
    }

}

function addGetOfferButton(eventStart, eventId, regionId) {
    var date = new Date(eventStart)
    var current = new Date
    if (current < date) {
        return '<button data-eventid="' + eventId + '" data-region="' + regionId + '" class="btn btn-primary getOfferButton">Get offer</button>'
    }
    else {
        return ""
    }

}

function showParticipantForm() {
    $("#participantForm").toggle();
    loadCities()
}

function addParticipant() {
    $("#submitParticipantInfo")
        .click(
            function () {
                var firstName = $("#firstname").val();
                var lastName = $("#lastname").val();
                var email = $("#email").val();
                var cityID = $("#citiesSelect").val();

                if ((firstName !== "" && firstName !== undefined)
                    && (lastName !== "" && lastName !== undefined)
                    && (email !== "" && email !== undefined)
                    && (cityID !== "" && cityID !== undefined)) {
                    saveParticipantAsUser(firstName, lastName, email, cityID)
                } else {
                    $("#eventMsg2")
                        .text(
                            "Can't save incorrect data...")
                }
            })
}

function saveParticipantAsUser(firstName, lastName, email, cityID) {
    var postData = {
        "first_name": firstName,
        "last_name": lastName,
        "email": email,
        "id": 0,
        "def_location": {"id": cityID}
    };

    console.log(postData);
    $.ajax({
        url: backendPath + "/users",
        method: "POST",
        data: JSON.stringify(postData),
        contentType: "application/json;charset=utf-8"
    }).done(function () {
        $("#eventMsg2").text("Participant/user saved successfully")
        //$("#participantForm").hide()
        //location.reload()
    }).error(function () {
        $("#eventMsg2").text("Participant not save, ERROOOR!")
    })

}


function parseParticipants(participantJSON) {
    if (participantJSON.length > 0) {
        var participantTableContent = "";
        var participantRowTemplate = "<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>";

        $.each(participantJSON, function (i, participant) {
            participantTableContent += sprintf(participantRowTemplate,
                participant.user.first_name + " " + participant.user.last_name,
                participant.user.email,
                participant.startLocation.name,
                '<button class="btn-xs participantDeleteButton" data-participantid="'+participant.id+'">x</button>')
        });


        var participantTableFrame = "<table id=\"eventsTable\">" +
            "<tr>" +
            "<th>Name</th>" +
            "<th>E-mail</th>" +
            "<th>Start location</th>" +
            "<th></th>" +
            "</tr>{0}</table>";

        return sprintf(participantTableFrame, participantTableContent);
    } else return "no participants";
}

function deleteParticipant(participantId) {
    $.ajax({
        url: backendPath + "/participants/del/"+participantId,
        method: "DELETE",
      //  contentType: "application/json;charset=utf-8"
    }).done(function () {
        refreshEventsTable();
    })

}

function parseVenue(venueJSON) {
    if (venueJSON !== undefined) {
        return sprintf("{0}<br>{1}<br>{2}<br>", venueJSON.name, venueJSON.facilities, venueJSON.city.name);
    } else return "no venue selected";
}


function loadOffer(eventid, regionid) {
    $.ajax({
        url: backendPath + "/offers/" + eventid + "/" + regionid
    }).done(function (data) {
        //console.log(JSON.stringify(data))
        showOffer(data);
        /*showPage("#offerpage",function() {
            showOffer(data);
        })*/

    })
}


function showOffer(offerJSON) {

    var table = '';
    for (event in offerJSON) {
        if (offerJSON[event] instanceof Object) {
            table += formatEvent(offerJSON[event]);
        }
    }
    $("#offerContent").html(table)
    showPage("#offerPage", function () {
        console.log("offer");
    });
}

function formatEvent(e) {
    var eventHTML =
        '<div class="event" data-id="{0}">' +
        '<div class="row">' +
        '<div class="col-md-4 eventTitle">Event title: {1}</div>' +
        '<div class="col-md-4 eventTimes">' +
        '<time class="col-md-6 eventStartDate">Start date:{3}</time>' +
        '<time class="col-md-6 eventEndDate">End date: {4}</time>' +
        '</div>' +
        '<div class="col-md-4 eventBudget">Budget: {5}</div></div>' +
        '{2}' +
        '<div class="allEventParticipants">{6}</div>' +
        '</div>';
    return sprintf(eventHTML, e.id, e.title, formatVenue(e.venue), e.fromDate, e.toDate, e.budget, formatAllParticipants(e.participants))
}

function formatVenue(v) {
    var venueHTML = '<div class="row eventVenue">' +
        '<div class="col-md-4">Venue: {0}</div>' +
        '<div class="col-md-4">Places:{1}</div>' +
        '<div class="col-md-4">Facilities: {2}</div>' +
        '</div>';
    return sprintf(venueHTML, v.name, v.places, v.facilities);
}

function formatAllParticipants(participantsJSON) {
    var retVal = '';
    for (var oneParticipant in participantsJSON) {
        retVal += formatOneParticipant(participantsJSON[oneParticipant]);
    }
    return retVal;
}

function formatOneParticipant(p) {
    var oneParticipantHTML =
        '<div class="row oneParticipant" data-id="{0}">' +
        '<div class="col-md-1">Name: {1}</div>' +
        '<div class="col-md-4 arrivalFlight">Arrival flight:  {2}</div>' +
        '<div class="col-md-4 departureFlight">Departure flight: {3}</div>' +
        '<div class="col-md-3">Hotel: {4}</div>' +
        '</div>'
    return sprintf(oneParticipantHTML, p.id, p.user.last_name + ", " + p.user.first_name, formatOneFlight(p.arrivalFlightBooking), formatOneFlight(p.departureFlightBooking), formatHotel(p.hotelbooking));
}


function formatOneFlight(f) {
    var flightHTML = '<div class="oneFlight">' +
        '<div class="row flightStartLoc">Departure time: {0}, from: {1}, {2}, {3}</div>' +
        '<div class="row flightEndLoc">Arrival time: {4}, at: {5}, {6}, {7}</div>' +
        '</div>';
    var retVal = sprintf(flightHTML,
        f.departure_time, f.from_airport.airport_code, f.cityFromName.name, f.cityFromName.in_country.name,
        f.arrival_time, f.to_airport.airport_code, f.cityToName.name, f.cityToName.in_country.name);
    return retVal;
}

function formatHotel(h) {
    var hotelHTML = '<div class="row hotel">' +
        '<div class="col-md-6">Name: {0}</div>' +
        '<div class="col-md-6">Room name: {1}</div>' +
        '</div>';
    return sprintf(hotelHTML, h.hotel.name, h.room_name)
}


function formatAllFlights(flightsJSON) {
    console.log('formatAllFlights: ', flightsJSON);
    return "<p><b>All FLIGHTs" + formatOffer(flightsJSON) + "</b></p>/allflights<hr>";
}