$(document).ready(function() {
	loadCities(loadCitiesCallback);

	$("#fromCity").change(function(data) {
		loadAirports(this.value, $("#fromAirport"))
	});
	$("#toCity").change(function(data) {
		loadAirports(this.value, $("#toAirport"))
	});
	$("#loadFromToCityByArrivalToTable").click(function() {
        loadDataForFromToCityByArrivalToTable();
    });
});

function loadCitiesCallback(data) {
	var fromCitySelect = $("#fromCity").html('');
	var toCitySelect = $("#toCity").html('');

	$.each(data, function(i, city) {
		var option = "<option value = '" + city.id + "'>" + city.name
				+ "</option>";
		fromCitySelect.append(option);
		toCitySelect.append(option);
	});
	fromCitySelect.change();
	toCitySelect.change();
}

var cities = null;
var flights = null;

function loadAirports(cityID, $selectID) {
	$.ajax({
		url : "http://localhost:8080/travify/rest/airports/byCity/" + cityID
	}).done(function(data) {
		console.log("loadairports done", cityID, $selectID, data);
		$selectID.html('');
		$.each(data, function(i, airport) {
			var $option = $('<option>', {
				value : airport.id,
				text : airport.airport_code
			});
			$selectID.append($option);
		})
	})
}

function loadCities(callback) {
	console.log("fromCitySelect");
	$.ajax({
		url : "http://localhost:8080/travify/rest/cities"
	}).done(function(data) {
		cities = data;
		console.log(data);
		if (callback !== undefined) {
			callback(data)
		}
	})
}

function loadDataForFromToCityByArrivalToTable(CityToID, CityFromID) {
	$.ajax({
		url : "http://localhost:8080/travify/rest/flights/arrivals/" + CityToID +"/"+ CityFromID
		
	}).done(
			function(data) {
				var table = '';
				$.each(data, function(i, allflightsdata) {
					console.log(allflightsdata);
					table += '<tr><td>' + allflightsdata.cityFromName.name
							+ ' </td> ' + '<td>'
							+ allflightsdata.from_airport.airport_code
							+ ' </td>' + '<td>' + allflightsdata.departure_time
							+ ' </td> ' + '<td>'
							+ allflightsdata.cityToName.name + '</td> '
							+ '<td>' + allflightsdata.to_airport.airport_code
							+ ' </td> ' + '<td>' + allflightsdata.arrival_time
							+ '</td> ' + '<td> ' + allflightsdata.price
							+ ' </td></tr> '
				});
//	ERIK: MERGE konflikt, ei teadnud kas vajalik, kommenteerisin välja.
// $(".fromToCityByArrivalTimeResults").empty(table);
				$(".fromToCityByArrivalTime").append(table);
				})
}

function selectArrivalCityFunction() {
	var selectedToCity = $("#toCity").val();
	var selectedFromCity = $("#fromCity").val();
	$("#fromToCityByArrivalTime").html(selectedToCity + selectedFromCity);
	console.log(selectedToCity)
	loadDataForFromToCityByArrivalToTable(selectedToCity, selectedFromCity)
}