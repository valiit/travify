var backendPath = 'http://localhost:8080/travify/rest'; // no slash (/) in the
// end!

if (!window.jQuery) {
	console.error("ERROR: no jQuery loaded");
}

$(document)
		.ready(
				function() {

					$("#loginBtn").off("click").click(function() {
						postLoginForm()
					});

					initDatePickers();

					loadAllEvents();

					$("#startLocation").html(generateCitiesSelect("citiesSelect"))

					/*
					 * $("#addParticipantBtn").click(function () {
					 * 
					 * $("#participantForm").toggle(); });
					 */

					$("#addEventBtn").off("click").click(function() {
						$("#eventStartForm").toggle();
						loadRegions();
					});

					$("#saveButton").
							off("click").click(
									function() {
										var title = $("#eventName").val();
										var regionID = $("#regionSelect").val();
										var fromDate = $("#fromDate").val();
										var toDate = $("#toDate").val();
										var arrivalFlex = $("#arrivalFlex")
												.val();
										var departureFlex = $("#departureFlex")
												.val();
										if ((title !== "" && title !== undefined)
												&& (regionID !== "" && regionID !== undefined)
												&& (fromDate !== "" && fromDate !== undefined)
												&& (toDate !== "" && toDate !== undefined)
												&& (arrivalFlex !== "" && arrivalFlex !== undefined)
												&& (departureFlex !== "" && departureFlex !== undefined)
												&& (fromDate < toDate)) {
											saveEvent(title, regionID,
													fromDate, toDate,
													arrivalFlex, departureFlex)
										} else {
											$("#eventMsg")
													.text(
															"Can't save incorrect data...")
										}
									})

					$("#loadOffer").off("click").click(function() {
						loadOffer();
					})

				});

function postLoginForm() {
	var loginData = {
		"userName" : $("#userName").val(),
		"password" : $("#password").val()
	};
	$.post({
		url : backendPath + "/users/login",
		data : loginData
	}).success(function(data) {
		if (data === "true") {
			window.location = 'participants.html';
		}
	})

}

function initDatePickers() {
	if ($(".dateTimePicker").length > 0) {
		jQuery.datetimepicker.setLocale('et');

		jQuery('#fromDate').datetimepicker({
			timepicker : true,
			format : 'Y-m-d H:i:00'
		});
		jQuery('#toDate').datetimepicker({
			timepicker : true,
			format : 'Y-m-d H:i:00'
		});
	}
}

function loadRegions() {
	$.ajax({
		url : backendPath + "/regions"
	}).done(function(data) {
		var $regionsSelect = $("#regionSelect").html('');
		$.each(data, function(i, itemData) {
			var $option = $('<option>', {
				value : itemData.id,
				text : itemData.region_name
			});
			$regionsSelect.append($option);
		});
		$regionsSelect.change();
	})

};

var cities;

function getAllCities() {
	$.ajax({
		url : backendPath + "/cities"
	}).done(function(data) {
		cities = data;
	});
}

function generateCitiesSelect(id, selectedId) {

	if (cities === undefined) {
		getAllCities();
	}

	var citiesSelect = "<select id=\"" + id + "\">";

	$.each(cities, function(i, itemData) {
		var selected = "";
		if (itemData.id === selectedId) {
			selected = " selected";
		}
		citiesSelect += "<option value = '" + itemData.id + "'" + selected
				+ ">" + itemData.name + "</option>";
	});
	citiesSelect += "</select>";

	return citiesSelect;
}

function saveEvent(title, regionID, fromDate, toDate, arrivalFlex,
		departureFlex) {

	/*
	 * ERIK! Ära kustuta ära seda välja kommitud koodi! ArrivalFlex = new Date
	 * (Date.parse(fromDate) - (arrivalFlex*24*60*60*1000)) departureFlex = new
	 * Date (Date.parse(toDate) + (departureFlex*24*60*60*1000))
	 */

	var postData = {
		"title" : title,
		"fromDate" : fromDate.replace(' ', 'T'),
		"toDate" : toDate.replace(' ', 'T'),
		"id" : 0,
		"arrivalFlex" : arrivalFlex * 24,
		"departureFlex" : departureFlex * 24,
		"region" : {"id" : regionID}
	};

	console.log(postData);
	$.ajax({
		url : backendPath + "/events",
		method : "POST",
		data : JSON.stringify(postData),
		contentType : "application/json;charset=utf-8"
	}).done(function() {
		$("#eventMsg").text("Event saved successfully")
		$("#eventStartForm").hide()
		location.reload()
	}).error(function() {
		$("#eventMsg").text("Event not save, ERROOOR!")
	})

}

function loadAllEvents() {
	$.ajax({
		url : backendPath + "/events"
	}).done(function(data) {		
		$('#eventsContainer').html(parseEvents(data))
		$(".addParticipantButton").off("click").click(function() {
			getParticipantsFromClick($(this).data('eventid'));
		})
	})
}

function sprintf() {
	var input = arguments[0];
	for (var i = 1; i < arguments.length; i++) {
		var toReplace = new RegExp("\{([" + (i - 1) + "])\}", "g")
		input = input.replace(toReplace, arguments[i]);
	}
	return input;
}

function reformatDate(date) {
	return date.replace('T', '<br>');
}

function parseEvents(eventsDataJSON) {
	var eventsTableContent = "";
	var eventRowTemplate = "<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td></tr>";

	$.each(eventsDataJSON, function(i, oneEvent) {
		// console.log(oneEvent);
		eventsTableContent += sprintf(eventRowTemplate, oneEvent.title,
				reformatDate(oneEvent.fromDate), reformatDate(oneEvent.toDate),
				parseVenue(oneEvent.venue),
				parseParticipants(oneEvent.participants),
				addParticipantButton(oneEvent.fromDate, oneEvent.id))
	});

	var eventsTableFrame = "<table id=\"eventsTable\">" + "<tr>"
			+ "<th>Title</th>" + "<th>From</th>" + "<th>To</th>"
			+ "<th>Events</th>" + "<th>Participants</th>" + "</tr>{0}</table>";

	return sprintf(eventsTableFrame, eventsTableContent);
}


function addParticipantButton(eventStart, eventId) {

	var date = new Date(eventStart)
	var current = new Date
	if (current < date) {
		console.log(eventId)
		return '<button data-eventId="' +eventId+ '" class="addParticipantButton" class="btn btn-secondary">Add participant</button>'
	} else {
		return "event closed"
	}

}

function getParticipantsFromClick(eventId) {
	$("#participantForm").toggle();
	loadAllUsers(eventId);
	getAllCities();
	
	addParticipantInfoClick();
}

function loadAllUsers(eventId) {
	$.ajax({
		url : backendPath + "/users"
	}).done(function(data) {
		$('#allUsers').html(parseUsers(data,eventId))
		$("#addParticipantToEvent").off("click").click(function() {
			addUsersToEvent()
		})
		})
}

function parseUsers(usersDataJSON,eventId) {
	var usersTableContent = "";
	var usersRowTemplate = "<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td></tr>";

	$.each(usersDataJSON, function(i, oneUser) {
	

		usersTableContent += sprintf(usersRowTemplate, oneUser.first_name,
				oneUser.last_name, oneUser.email, generateCitiesSelect(
						"citiesSelect-"+oneUser.id, oneUser.def_location.id),
				'<input id="check" type="checkbox" data-eventid="'+eventId +'" data-id="' +oneUser.id+ '"></input>')
	});
	var usersTableFrame = "<table id=\"usersTable\">" + "<tr>"
			+ "<th>First name</th>" + "<th>Last name</th>" + "<th>E-mail</th>"
			+ "<th>Default location</th>" + "<th><button id=\"addParticipantToEvent\">Add Participants to Event</button></th>"+ "</tr>{0}</table>";
	return sprintf(usersTableFrame, usersTableContent);
}



function getAllCheckedUsers(){
	
	var users = [];
	$("#usersTable").find("input#check").each(function(i,item) {
		if ($(item).is(":checked")) { 
			var userId = $(item).data("id");
			var eventId = $(item).data("eventid");
			var userLoc = $("select#citiesSelect-"+userId).val();
			
			var user = {
					"user" : {"id" : userId},
					"startLocation" : {"id" : userLoc},
					"eventId" : eventId
			}
			users.push(user);
		} 
	})
	console.log(JSON.stringify(users))
	return users;
}
	

function addUsersToEvent() {
	var allUsers = getAllCheckedUsers();
	 $.ajax({
		 url : backendPath + "/participants/addmultipleparticipants",
		 method : "POST",
		 data : JSON.stringify(allUsers),
		 contentType : "application/json;charset=utf-8"
	 }).done(function() {
			console.log("Users added to Event!")
			location.reload()
		}).error(function() {
			console.log("Can't save users to Event!")
		})
}




function addParticipantInfoClick() {
	$("#startLocation").html(generateCitiesSelect("citiesSelect"))
	$("#submitParticipantInfo").off("click").click(
			function() {
				var firstName = $("#firstname").val();
				var lastName = $("#lastname").val();
				var email = $("#email").val();
				var cityID = $("#citiesSelect").val();

				if ((firstName !== "" && firstName !== undefined)
						&& (lastName !== "" && lastName !== undefined)
						&& (email !== "" && email !== undefined)
						&& (cityID !== "" && cityID !== undefined)) {
						saveParticipantAsUser(firstName, lastName, email,
								cityID)								
				} else {
					$("#eventMsg2").text("Can't save incorrect data...")
				} 

			})
}
function saveParticipantAsUser(firstName, lastName, email, cityID) {
	var postData = {
		"first_name" : firstName,
		"last_name" : lastName,
		"email" : email,
		"def_location" : {
			"id" : cityID
		}
	};
	
	$.ajax({
		url : backendPath + "/users/checkIfUserExists",
		method : "POST",
		data : JSON.stringify(postData),
		contentType : "application/json;charset=utf-8"
	}).done(function(isExisting) {
			if(isExisting == "false") {
			$.ajax({
			url : backendPath + "/users",
			method : "POST",
			data : JSON.stringify(postData),
			contentType : "application/json;charset=utf-8"
			}).done(function(data) {
				$("#eventMsg2").text("Participant/user saved successfully")
				loadAllUsers();
			})

	
		} else {
			$("#eventMsg2").text("User already exists. Pick from above!")
		}
	}).error(function() {
		$("#eventMsg2").text("Participant not save, ERROOOR!")
	})

}

function parseParticipants(participantJSON) {
	if (participantJSON.length > 0) {
		var participantTableContent = "";
		var participantRowTemplate = "<tr><td>{0}</td><td>{1}</td><td>{2}</td></tr>";

		$.each(participantJSON, function(i, participant) {
			participantTableContent += sprintf(participantRowTemplate,
					participant.user.first_name + " "
							+ participant.user.last_name,
					participant.user.email, participant.startLocation.name)
		});

		var participantTableFrame = "<table id=\"eventsTable\">" + "<tr>"
				+ "<th>Name</th>" + "<th>E-mail</th>" + "<th>Start point</th>"
				+ "</tr>{0}</table>";

		return sprintf(participantTableFrame, participantTableContent);
	} else
		return "no participants";
}

function parseVenue(venueJSON) {
	if (venueJSON !== undefined) {
		return sprintf("{0}<br>{1}<br>{2}<br>", venueJSON.name,
				venueJSON.facilities, venueJSON.city.name);
	} else
		return "no venue selected";
}

function loadOffer() {
	$.ajax({
		url : backendPath + "/offers/1/6"
	}).done(function(data) {
		showOffer(data);
	})
}

function scan_b_works(obj, line) {
	var retVal = '';
	if (obj instanceof Object) {
		line += '<ul>'
		for ( var k in obj) {
			if (obj.hasOwnProperty(k)) {
				retVal += "<ul><li>" + k + ": " + scan(obj[k], line)
						+ "</li></ul>";
			}
		}
	} else {
		// not an Object so obj[k] here is a value
		retVal += obj
	}
	;

	return retVal;

};

function scan(obj, line) {
	var retVal = '';
	if (obj instanceof Object) {
		line += '<ul>'
		for ( var k in obj) {
			if (obj.hasOwnProperty(k)) {
				console.log("k:", k, " obj[k]:", obj[k]);
				retVal += "<table style=\"background-color:" + randomColor()
						+ "\" class=\"" + k + "\"><tr><td><b>" + k
						+ "</b></td><td>" + scan(obj[k], line)
						+ "</td></tr></table>";
			}
		}
	} else {
		// not an Object so obj[k] here is a value
		retVal += obj
	}
	;

	return retVal;

};

function showOffer(offerJSON) {
	// var table = scan(offerJSON,'')
	var table = formatOffer(offerJSON, '')
	// console.log(table);
	$("#offer").html(table);
}

function formatOffer(offerJSON) {
	var retVal = '';
	if (offerJSON instanceof Object) {
		for ( var k in offerJSON) {
			if (k in [ "arrivalFlightBooking", "departureFlightBooking" ]) {
				retVal += formatOneFlight(offerJSON[k]);
			} else if (k === "event") {
				retVal += formatEvent(offerJSON[k])
			} else if (k in [ "venue" ]) {
				retVal += formatVenue(offerJSON[k])
			} else if (k === "participants") {
				retVal += formatAllParticipants(offerJSON[k])
			} else if (k === "participant") {
				retVal += formatOneParticipant(offerJSON[k])
			} else if (offerJSON.hasOwnProperty(k)) {
				retVal += "<table style=\"background-color:" + randomColor()
						+ "\" class=\"" + k + "\"><tr><td><b>" + k
						+ "</b></td><td>" + formatOffer(offerJSON[k])
						+ "</td></tr></table>";
			}
		}
	} else {
		// not an Object so obj[k] here is a value
		retVal += offerJSON
	}
	;

	return retVal;
}

function formatEvent(e) {
	var participantsHTML = '<div class="allEventParticipants">{0}</div>';
	participantsHTML = sprintf(participantsHTML,
			formatAllParticipants(e.participants));

	var venueHTML = '<div class="eventVenue">{0}</div>';
	venueHTML = sprintf(venueHTML, formatVenue(e.venue))

	var eventHTML = '<article class="event" data-id="{0}">'
			+ '<summary>{1}</summary>'
			+ '<div class="eventTitle">Event title: {1}</div>'
			+ '<div class="eventVenueName">Venue name: {2}</div>'
			+ '<time class="eventStartDate">Start date:{3}</time>'
			+ '<time class="eventEndDate">End date: {4}</time>'
			+ '<div class="eventBudget">Budget: {5}</div>'
			+ '<details>{6}{7}</details></article>';

	return sprintf(eventHTML, e.id, e.title, e.venue.name, e.fromDate,
			e.toDate, e.budget, participantsHTML, venueHTML)
}

function formatVenue(v) {
	var venueHTML = '<article class="eventVenue"><summary>Venue: {0}</summary><details>({1} places). Facilities: {2}</details></article>';
	return sprintf(venueHTML, v.name, v.places, v.facilities);
}

function formatAllParticipants(participantsJSON) {
	var retVal = '';
	for ( var oneParticipant in participantsJSON) {
		retVal += formatOneParticipant(participantsJSON[oneParticipant]);
	}
	return retVal;
}

function formatOneParticipant(p) {
	var departureFlightHTML = formatOneFlight(p.departureFlightBooking);
	var arrivalFlightHTML = formatOneFlight(p.arrivalFlightBooking);
	var hotelHTML = formatHotel(p.hotelbooking)
	var oneParticipantHTML = '<article class="oneParticipant" data-id="{0}">'
			+ '<summary>Name: {1}</summary><details>'
			+ 'Arrival flight: <div class="arrivalFlight">{2}</div>'
			+ 'Departure flight: <div class="departureFlight">{3}</div>'
			+ 'Hotel: {4}</details>' + '</article>'
	return sprintf(oneParticipantHTML, p.id, p.user.last_name + ", "
			+ p.user.first_name, arrivalFlightHTML, departureFlightHTML,
			hotelHTML);
}

function formatOneFlight(f) {
	var flightHTML = '<div class="oneFlight">'
			+ '<div class="flightStartLoc">Departure time: {0}, from: {1}, {2}, {3}</div><br>'
			+ '<div class="flightEndLoc">Arrival time: {4}, at: {5}, {6}, {7}</div><br>'
			+ '</div>';
	var retVal = sprintf(flightHTML, f.departure_time,
			f.from_airport.airport_code, f.cityFromName.name,
			f.cityFromName.in_country.name, f.arrival_time,
			f.to_airport.airport_code, f.cityToName.name,
			f.cityToName.in_country.name);
	return retVal;
}

function formatHotel(h) {
	var hotelHTML = '<div class="hotel">' + 'Name: {0}<br>'
			+ 'Room name: {1}</div>';
	return sprintf(hotelHTML, h.hotel.name, h.room_name)
}

function formatAllFlights(flightsJSON) {
	console.log('formatAllFlights: ', flightsJSON);
	return "<p><b>All FLIGHTs" + formatOffer(flightsJSON)
			+ "</b></p>/allflights<hr>";
}